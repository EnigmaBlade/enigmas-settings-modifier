package net.enigmablade.lol.lolsetting.util;

import java.awt.*;
import java.util.*;

public class SettingsUtil
{
	public static DisplayMode[] getResolutions()
	{
		LinkedList<DisplayMode> resolutions = new LinkedList<DisplayMode>();
		
		GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice device = env.getDefaultScreenDevice();
		DisplayMode displayModes[] = device.getDisplayModes();
		for(DisplayMode mode : displayModes)
		{
			//System.out.println("Checking mode: "+mode.getWidth()+"x"+mode.getHeight()+"x"+mode.getBitDepth()+" ("+mode.getRefreshRate()+" Hz)");
			if(mode.getBitDepth() == 32 && mode.getRefreshRate() == 60 && mode.getWidth() >= 1024 && mode.getHeight() >= 768)
			{
				boolean noDup = true;
				for(DisplayMode m : resolutions)
					if(mode.getWidth() == m.getWidth() && mode.getHeight() == m.getHeight())
						noDup = false;
				if(noDup)
					resolutions.add(mode);
			}
		}
		
		return resolutions.toArray(new DisplayMode[resolutions.size()]);
	}
	
	public static DisplayMode getCurrentResolution()
	{
		GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice device = env.getDefaultScreenDevice();
		return device.getDisplayMode();
	}
	
	public static DisplayMode getSpecificResolution(int width, int height)
	{
		DisplayMode[] modes = getResolutions();
		for(DisplayMode mode : modes)
			if(mode.getWidth() == width && mode.getHeight() == height)
				return mode	;
		return null;
	}
}
