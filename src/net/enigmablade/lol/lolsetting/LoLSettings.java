package net.enigmablade.lol.lolsetting;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.Timer;

import net.enigmablade.paradoxion.ui.*;
import static net.enigmablade.paradoxion.util.Logger.*;

import net.enigmablade.lol.lollib.io.*;
import net.enigmablade.lol.lollib.io.pathhelpers.platforms.*;
import net.enigmablade.lol.lollib.io.pathhelpers.regions.*;
import net.enigmablade.lol.lollib.ui.dialogs.*;

import net.enigmablade.lol.lolsetting.data.*;
import net.enigmablade.lol.lolsetting.io.*;
import net.enigmablade.lol.lolsetting.ui.*;
import net.enigmablade.lol.lolsetting.ui.dialogs.*;
import net.enigmablade.lol.lolsetting.util.*;

public class LoLSettings
{
	public static final String appName = "Enigma's Settings Modifier";
	public static final String appKey = "EnigmaSetting";
	public static final String version = "1.0.1", buildVersion = "0", versionAdd = "";
	
	private LoLSettingsUI ui;
	
	private Settings settings;
	
	//Options
	private Deque<GamePath> lolDirHistory;
	private Preferences options;
	
	public LoLSettings()
	{
		writeToLog("App info: "+appName+" ("+appKey+"), v"+version+"b"+buildVersion+" "+versionAdd);
		
		UpdateUtil.finishUpdate();
		
		writeToLog("Begin startup initialization");
		
		options = Preferences.load();
		
		if(options.checkVersion)
		{
			//SplashScreen.drawString("Checking version...");
			UpdateUtil.startUpdater(appKey, version, buildVersion, false);
		}
		
		//SplashScreen.drawString("Loading resources...");
		//LocaleDatabase.loadLocales("resources.locales", languages);
		//LocaleDatabase.setLocale(currentLanguage);
		
		//SplashScreen.drawString("Finding paths...");
		writeToLog("Initializing LoL file IO");
		if(GamePathUtil.initialize(options.lolDirPath, Region.stringToRegion(options.lolDirRegion)))
		{
			options.lolDirPath = GamePathUtil.getDir().getPath();
			options.lolDirRegion = GamePathUtil.getDir().getRegion().toString().toLowerCase();
			lolDirHistory = new ArrayDeque<GamePath>();
		}
		else
		{
			writeToLog("Failed to retrieve a directory path, cannot continue", LoggingType.ERROR);
			System.exit(1);
		}
		
		//if(!createLock() && JOptionPane.showConfirmDialog(this, "There is another instance of the item changer currently running.\nMultiple instances could cause a loss of saved data.\n\nDo you want to launch the additional instance?", "Instance locked", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) == JOptionPane.NO_OPTION)
		//	SystemUtil.exit(0);
		
		//SplashScreen.drawString("Loading data...");
		writeToLog("Initializing data");
		loadSettings();
		
		//SplashScreen.drawString("Loading UI...");
		writeToLog("Initializing UI");
		setupUI();
		updateSettingsUI();
		
		writeToLog("Startup initialization complete");
		//checkGameVersion();
		writeToLog("------------------------------------");
	}
	
	private void setupUI()
	{
		ui = new LoLSettingsUI(this, appName, version+" "+versionAdd, options);
	}
	
	//Data methods
	
	public void loadSettings()
	{
		settings = SettingsIO.loadSettings();
	}
	
	public void updateSettingsUI()
	{
		//Video settings
		ui.setResolutions(SettingsUtil.getResolutions());
		ui.setRecommendedResolution(SettingsUtil.getCurrentResolution());
		ui.setSelectedResolution(SettingsUtil.getSpecificResolution(settings.getInt("video.width"), settings.getInt("video.height")));
		ui.setWindowMode(settings.getInt("video.windowMode"));
		ui.setVerticalSync(settings.getBoolean("video.vsync"));
		ui.setFramerate(settings.getInt("video.framerate"));
		
		ui.setCharacterQuality(settings.getInt("video.characterQuality"));
		ui.setEnvironmentQuality(settings.getInt("video.environmentQuality"));
		ui.setEffectQuality(settings.getInt("video.effectsQuality"));
		ui.setShadowQuality(settings.getInt("video.shadowQuality"));
		ui.setAdvancedLighting(settings.getBoolean("video.advancedLighting"));
		ui.setAdvancedReflections(settings.getBoolean("video.advancedReflections"));
		ui.setParticleOptimizations(settings.getBoolean("video.particleOptimizations"));
		
		//Audio settings
		ui.setMasterVolume(settings.getFloat("audio.master"));
		ui.setMusicVolume(settings.getFloat("audio.music"));
		ui.setSfxVolume(settings.getFloat("audio.sfx"));
		ui.setVoiceVolume(settings.getFloat("audio.voice"));
		ui.setAnnouncerVolume(settings.getFloat("audio.announcer"));
		
		//Interface settings
		ui.setUseNewHud(settings.getBoolean("hud.hudNew"));
		
		ui.setHudScale(settings.getFloat("hud.hudScale"));
		ui.setHideWall(!settings.getBoolean("hud.drawWall"));
		ui.setHudAnimations(settings.getBoolean("hud.animations"));
		
		ui.setShowNewbieTips(settings.getBoolean("hud.newbieTips"));
		ui.setShowHealthBars(settings.getBoolean("hud.healthBars"));
		ui.setCooldownDisplay(settings.getInt("hud.cooldownDisplay"));
		
		ui.setShopDisplayMode(settings.getInt("hud.shopDisplayMode"));
		ui.setShopStartPane(settings.getInt("hud.shopStartPane"));
		
		ui.setMinimapScale(settings.getFloat("hud.minimapScale"));
		ui.setMinimapFlipped(settings.getBoolean("hud.flipMinimap"));
		ui.setShowNeutral(settings.getBoolean("hud.showNeutral"));
		
		ui.setChatScale(settings.getFloat("hud.chatScale"));
		ui.setShowAllChat(settings.getBoolean("hud.showAllChat"));
		ui.setShowTimestamps(settings.getBoolean("hud.showTimestamps"));
		
		//Game settings
		ui.setColorblind(settings.getBoolean("game.colorblind"));
		ui.setDamageFlash(settings.getBoolean("game.damageFlash"));
		ui.setRespawnSnap(settings.getBoolean("game.respawnsnap"));
		ui.setMovePrediction(settings.getBoolean("game.movePredict"));
		ui.setLineMissileDisplay(settings.getBoolean("game.lineMissileDisplay"));
		ui.setSmartcastRangeIndicators(settings.getBoolean("game.smartcastRange"));
		ui.setScrollSpeed(settings.getFloat("game.scrollSpeed"));
		ui.setSmoothScrolling(settings.getBoolean("game.scrollSmoothing"));
		
		//Floating text
		ui.setLegacyText(settings.getBoolean("float.legacy"));
		ui.setDamageText(settings.getBoolean("float.damage"));
		ui.setEnemyDamageText(settings.getBoolean("float.enemyDamage"));
		ui.setCritText(settings.getBoolean("float.crit"));
		ui.setEnemyCritText(settings.getBoolean("float.enemyCrit"));
		ui.setHealText(settings.getBoolean("float.heal"));
		ui.setDodgeText(settings.getBoolean("float.dodge"));
		ui.setGoldText(settings.getBoolean("float.gold"));
		ui.setAllyGoldText(settings.getBoolean("float.allyGold"));
		ui.setLevelText(settings.getBoolean("float.level"));
		ui.setStatusText(settings.getBoolean("float.status"));
		ui.setSpecialText(settings.getBoolean("float.special"));
		ui.setQuestText(settings.getBoolean("float.quest"));
		ui.setScoreText(settings.getBoolean("float.score"));
		ui.setManaText(settings.getBoolean("float.mana"));
		ui.setExperienceText(settings.getBoolean("float.experience"));
	}
	
	public void retrieveSettingsFromUI()
	{
		//Video settings
		settings.set("video.width", ui.getResolution().getWidth());
		settings.set("video.height", ui.getResolution().getHeight());
		settings.set("video.windowMode", ui.getWindowMode());
		settings.set("video.vsync", ui.getVerticalSync());
		settings.set("video.framerate", ui.getFramerate());
		
		settings.set("video.characterQuality", ui.getCharacterQuality());
		settings.set("video.environmentQuality", ui.getEnvironmentQuality());
		settings.set("video.effectsQuality", ui.getEffectsQuality());
		settings.set("video.shadowQuality", ui.getShadowQuality());
		settings.set("video.advancedLighting", ui.getAdvancedLighting());
		settings.set("video.advancedReflections", ui.getAdvancedReflections());
		settings.set("video.particleOptimizations", ui.getParticleOptimizations());
		
		//Audio settings
		settings.set("audio.master", ui.getMasterVolume());
		settings.set("audio.music", ui.getMusicVolume());
		settings.set("audio.sfx", ui.getSfxVolume());
		settings.set("audio.voice", ui.getVoiceVolume());
		settings.set("audio.announcer", ui.getAnnouncerVolume());
		settings.set("audio.enabled", ui.getAudioEnabled());
		
		//Interface settings
		settings.set("hud.hudNew", ui.getUseNewHud());
		
		settings.set("hud.hudScale", ui.getHudScale());
		settings.set("hud.drawWall", !ui.getHideWall());
		settings.set("hud.animations", ui.getHudAnimations());
		
		settings.set("hud.newbieTips", ui.getShowNewbieTips());
		settings.set("hud.healthBars", ui.getShowHealthBars());
		settings.set("hud.cooldownDisplay", ui.getCooldownDisplay());
		
		settings.set("hud.shopDisplayMode", ui.getShopDisplayMode());
		settings.set("hud.shopStartPane", ui.getShopStartPane());
		
		settings.set("hud.minimapScale", ui.getMinimapScale());
		settings.set("hud.flipMinimap", ui.getMinimapFlipped());
		settings.set("hud.showNeutral", ui.getShowNeutral());
		
		settings.set("hud.chatScale", ui.getChatScale());
		settings.set("hud.showAllChat", ui.getShowAllChat());
		settings.set("hud.showTimestamps", ui.getShowTimestamps());
		
		//Game settings
		settings.set("game.colorblind", ui.getColorblindEnabled());
		settings.set("game.damageFlash", ui.getDamageFlash());
		settings.set("game.respawnsnap", ui.getRespawnSnap());
		settings.set("game.movePredict", ui.getMovePrediction());
		settings.set("game.lineMissileDisplay", ui.getLineMissileDisplay());
		settings.set("game.smartcastRange", ui.getSmartcastRangeIndicators());
		settings.set("game.scrollSpeed", ui.getScrollSpeed());
		settings.set("game.scrollSmoothing", ui.getSmoothScrolling());
		
		//Floating text
		settings.set("float.legacy", ui.getLegacyText());
		settings.set("float.damage", ui.getDamageText());
		settings.set("float.enemyDamage", ui.getEnemyDamageText());
		settings.set("float.crit", ui.getCritText());
		settings.set("float.enemyCrit", ui.getEnemyCritText());
		settings.set("float.heal", ui.getHealText());
		settings.set("float.dodge", ui.getDodgeText());
		settings.set("float.gold", ui.getGoldText());
		settings.set("float.allyGold", ui.getAllyGoldText());
		settings.set("float.level", ui.getLevelText());
		settings.set("float.status", ui.getStatusText());
		settings.set("float.special", ui.getSpecialText());
		settings.set("float.quest", ui.getQuestText());
		settings.set("float.score", ui.getScoreText());
		settings.set("float.mana", ui.getManaText());
		settings.set("float.experience", ui.getExperienceText());
	}
	
	//GUI methods
	
	public void open()
	{
		ui.setVisible(true);
	}
	
	public void gui_close()
	{
		//checkSave();
		options.save();
	}
	
	public void gui_save()
	{
		writeToLog("Saving settings");
		
		retrieveSettingsFromUI();
		boolean success = SettingsIO.saveSettings(settings);
		String status = success ? "Settings saved" : "Settings failed to save";
		ui.setInfoText(status, !success);
		
		Timer t = new Timer(1500, new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				SwingUtilities.invokeLater(new Runnable(){
					@Override
					public void run()
					{
						ui.setInfoText("", false);
					}
				});
			}
		});
		t.setRepeats(false);
		t.start();
	}
	
	public void gui_reload()
	{
		writeToLog("Resetting settings");
		
		loadSettings();
		updateSettingsUI();
	}
	
	public void gui_resetDefault()
	{
		writeToLog("Resetting settings to default");
		
		//Video settings
		DisplayMode rec = SettingsUtil.getCurrentResolution();
		settings.set("video.width", rec.getWidth());
		settings.set("video.height", rec.getHeight());
		settings.set("video.windowMode", 0);
		settings.set("video.verticalSync", false);
		settings.set("video.framerate", 0);
		
		settings.set("video.characterQuality", 2);
		settings.set("video.environmentQuality", 2);
		settings.set("video.effectsQuality", 2);
		settings.set("video.shadowQuality", 2);
		settings.set("video.advancedLighting", true);
		settings.set("video.advancedReflections", true);
		settings.set("video.particleOptimizations", false);
		
		//Audio settings
		settings.set("audio.master", 1.0f);
		settings.set("audio.music", 0.5f);
		settings.set("audio.sfx", 1.0f);
		settings.set("audio.voice", 0.5f);
		settings.set("audio.announcer", 0.5f);
		settings.set("audio.enabled", true);
		
		//Interface settings
		settings.set("hud.hudNew", true);
		
		settings.set("hud.hudScale", 100f);
		settings.set("hud.drawWall", true);
		settings.set("hud.animations", true);
		
		settings.set("hud.newbieTips", true);
		settings.set("hud.healthBars", true);
		settings.set("hud.cooldownDisplay", 0);
		
		settings.set("hud.shopDisplayMode", 0);
		settings.set("hud.shopStartPane", 0);
		
		settings.set("hud.minimapScale", 100f);
		settings.set("hud.flipMinimap", false);
		
		settings.set("hud.chatScale", 100f);
		settings.set("hud.showAllChat", false);
		settings.set("hud.showTimestamps", false);
		
		//Game settings
		settings.set("game.colorblind", false);
		settings.set("game.damageFlash", true);
		settings.set("game.respawnsnap", false);
		settings.set("game.movePredict", true);
		settings.set("game.lineMissileDisplay", true);
		settings.set("game.smartcastRange", false);
		settings.set("game.scrollSpeed", 0.25f);
		settings.set("game.scrollSmoothing", true);
		
		//Floating text
		settings.set("float.legacy", false);
		settings.set("float.damage", true);
		settings.set("float.enemyDamage", true);
		settings.set("float.crit", true);
		settings.set("float.enemyCrit", true);
		settings.set("float.heal", true);
		settings.set("float.dodge", true);
		settings.set("float.gold", true);
		settings.set("float.level", true);
		settings.set("float.status", true);
		settings.set("float.special", true);
		settings.set("float.quest", true);
		settings.set("float.score", true);
		settings.set("float.mana", false);
		settings.set("float.experience", false);
		
		updateSettingsUI();
	}
	
	public void gui_resetRecommended()
	{
		writeToLog("Resetting settings to recommended");
		
		//Video settings
		DisplayMode rec = SettingsUtil.getCurrentResolution();
		settings.set("video.width", rec.getWidth());
		settings.set("video.height", rec.getHeight());
		settings.set("video.windowMode", 2);
		//settings.set("video.verticalSync", ui.getVerticalSync());
		//settings.set("video.framerate", ui.getFramerate());
		
		//settings.set("video.characterQuality", ui.getCharacterQuality());
		//settings.set("video.environmentQuality", ui.getEnvironmentQuality());
		//settings.set("video.effectsQuality", ui.getEffectsQuality());
		//settings.set("video.shadowQuality", ui.getShadowQuality());
		//settings.set("video.advancedLighting", ui.getAdvancedLighting());
		//settings.set("video.advancedReflections", ui.getAdvancedReflections());
		//settings.set("video.particleOptimizations", ui.getParticleOptimizations());
		
		//Audio settings
		settings.set("audio.master", 1.0f);
		settings.set("audio.music", 0.5f);
		settings.set("audio.sfx", 1.0f);
		settings.set("audio.voice", 0.5f);
		settings.set("audio.announcer", 0.75f);
		settings.set("audio.enabled", true);
		
		//Interface settings
		settings.set("hud.hudScale", 80f);
		settings.set("hud.drawWall", false);
		settings.set("hud.animations", true);
		
		settings.set("hud.newbieTips", false);
		settings.set("hud.healthBars", true);
		settings.set("hud.cooldownDisplay", 0);
		
		settings.set("hud.shopDisplayMode", 1);
		settings.set("hud.shopStartPane", 0);
		
		settings.set("hud.minimapScale", 100f);
		settings.set("hud.flipMinimap", false);
		
		settings.set("hud.chatScale", 80f);
		settings.set("hud.showAllChat", true);
		settings.set("hud.showTimestamps", true);
		
		//Game settings
		settings.set("game.colorblind", false);
		settings.set("game.damageFlash", true);
		settings.set("game.respawnsnap", false);
		settings.set("game.movePredict", false);
		settings.set("game.lineMissileDisplay", true);
		settings.set("game.smartcastRange", false);
		settings.set("game.scrollSpeed", 0.5f);
		settings.set("game.scrollSmoothing", true);
		
		//Floating text
		settings.set("float.legacy", false);
		settings.set("float.damage", true);
		settings.set("float.enemyDamage", true);
		settings.set("float.crit", true);
		settings.set("float.enemyCrit", true);
		settings.set("float.heal", true);
		settings.set("float.dodge", true);
		settings.set("float.gold", true);
		settings.set("float.level", true);
		settings.set("float.status", true);
		settings.set("float.special", true);
		settings.set("float.quest", true);
		settings.set("float.score", true);
		settings.set("float.mana", false);
		settings.set("float.experience", false);
		
		updateSettingsUI();
	}
	
	public void gui_export()
	{
		writeToLog("Exporting settings");
		
		//TODO
	}
	
	public void gui_import()
	{
		writeToLog("Importing settings");
		
		//TODO
	}
	
	//Options menu
	
	public void gui_setStartupVersionCheck(boolean check)
	{
		options.checkVersion = check;
	}
	
	public void gui_startUpdater()
	{
		UpdateUtil.startUpdater(appKey, version, buildVersion, true);
	}
	
	public void gui_openPathDialog()
	{
		GamePath gameDir = GamePathUtil.getDir();
		GamePath path = PathDialog.open(ui, Platform.getCurrentPlatform(), gameDir, lolDirHistory.toArray(new GamePath[0]));
		if(lolDirHistory.contains(path))
			lolDirHistory.remove(path);
		lolDirHistory.addFirst(path);
		if(lolDirHistory.size() > 5)
			lolDirHistory.removeLast();
		GamePathUtil.setLoLDir(path, true);
	}
	
	//Help menu
	
	public void gui_help()
	{
		GUIUtil.openURL("http://enigmablade.net/lol-settings-modifier/help.html");
	}
	
	public void gui_about()
	{
		AboutDialog.openDialog(ui, version+"b"+buildVersion+" "+versionAdd);
	}
	
	public void gui_changelog()
	{
		GUIUtil.openURL("http://enigmablade.net/lol-settings-modifier/changelog.html");
	}
	
	public void gui_donate()
	{
		GUIUtil.openURL("http://enigmablade.net/lol-settings-modifier/download.html");
	}
	
	public void gui_moreTools()
	{
		GUIUtil.openURL("http://enigmablade.net/");
	}
}
