package net.enigmablade.lol.lolsetting;

import java.io.*;
import java.util.*;
import javax.swing.*;
import net.enigmablade.paradoxion.util.Logger.*;
import static net.enigmablade.paradoxion.util.Logger.*;

public class Preferences
{
	//Options
	public boolean checkVersion = true;
	
	public String lolDirPath = null;
	public String lolDirRegion = null;
	
	//Data
	private static String FILE = "options.txt";
	
	public static Preferences load()
	{
		writeToLog("Loading options");
		File file = new File(FILE);
		if(!file.exists())
		{
			writeDefaults(file);
		}
		
		Preferences options = new Preferences();
		
		Scanner scanner = null;
		String line = null;
		try
		{
			scanner = new Scanner(file);
			while(scanner.hasNext())
			{
				line = scanner.nextLine().trim();
				int index = line.indexOf(':');
				String key = line.substring(0, index);
				String value = line.substring(index+1);
				if(key.equals("lolpath"))
				{
					if("null".equals(value))
						options.lolDirPath = null;
					else
						options.lolDirPath = value;
				}
				else if(key.equals("region"))
				{
					options.lolDirRegion = value;
				}
				else if(key.equals("updatestartup"))
				{
					int bool = Integer.parseInt(value);
					options.checkVersion = bool == 1;
				}
			}
		}
		catch(Exception e)
		{
			writeToLog("Failed to load options", LoggingType.ERROR);
			writeStackTrace(e);
			JOptionPane.showMessageDialog(null, "Error: Could not load options", "Error", JOptionPane.ERROR_MESSAGE);
		}
		finally
		{
			if(scanner != null)
				scanner.close();
		}
		
		return options;
	}
	
	private static void writeDefaults(File file)
	{
		PrintStream out = null;
		try
		{
			out = new PrintStream(file);
			out.println("lolpath:null");
			out.println("updatestartup:0");
		}
		catch(FileNotFoundException e)
		{
			writeToLog("Failed to create options file.", LoggingType.ERROR);
			writeStackTrace(e);
			JOptionPane.showMessageDialog(null, "Could not create options file", "Error", JOptionPane.ERROR_MESSAGE);
		}
		finally
		{
			if(out != null)
				out.close();
		}
	}
	
	public void save()
	{
		writeToLog("Saving options");
		File file = new File(FILE);
		PrintStream out = null;
		try
		{
			out = new PrintStream(file);
			out.println("lolpath:"+(lolDirPath != null ? lolDirPath : null));
			if(lolDirPath != null)
				out.println("lolregion:"+lolDirRegion);
			out.println("updatestartup:"+(checkVersion ? "1" : "0"));
		}
		catch(FileNotFoundException e)
		{
			writeToLog("Failed to save options", LoggingType.ERROR);
			writeStackTrace(e);
			//JOptionPane.showMessageDialog(ui, getString("dialog.options.errorSave"), getString("dialog.options.errorTitle"), JOptionPane.ERROR_MESSAGE);
		}
		finally
		{
			if(out != null)
				out.close();
		}
	}
}
