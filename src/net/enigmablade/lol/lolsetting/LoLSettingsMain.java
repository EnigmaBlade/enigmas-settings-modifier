package net.enigmablade.lol.lolsetting;

import javax.swing.*;

import net.enigmablade.paradoxion.io.*;
import net.enigmablade.paradoxion.util.*;
import static net.enigmablade.paradoxion.util.Logger.*;

public class LoLSettingsMain
{
	public static void main(String[] args)
	{
		boolean debugMode = false, consoleMode = false;
		
		//Parse command-line arguments
		for(int n = 0; n < args.length; n++)
		{
			String arg = args[n];
			if(arg.equals("-debug"))
			{
				debugMode = true;
			}
			else if(arg.equals("-console"))
			{
				consoleMode = true;
			}
			else
			{
				System.out.println("Unknown argument \""+arg+"\"");
			}
		}
		
		SystemUtil.initSystem(debugMode, consoleMode);
		
		//Splash
		//SplashScreen.init();
		//SplashScreen.drawString("Loading...");
		
		//Look and feel
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			//JFrame.setDefaultLookAndFeelDecorated(true);
		}
		catch(Exception e)
		{
			writeToLog("Failed to set the system look and feel", LoggingType.WARNING);
			writeStackTrace(e);
		}
		
		//Create a new instance and parse the build code if required
		try
		{
			LoLSettings main = new LoLSettings();
			
			//SplashScreen.close();
			main.open();
		}
		catch(Exception e)
		{
			writeToLog("Oh no! Unhandled exception!", LoggingType.ERROR);
			writeStackTrace(e);
			
			String text = e.getClass().toString();
			StackTraceElement[] trace = e.getStackTrace();
			for(int n = 0; n < Math.min(trace.length, 8); n++)
				text += "\n    "+trace[n].toString();
			Object[] options = {"Copy error", "Ok..."};
			if(JOptionPane.showOptionDialog(null, "Oh no! It broke!\n\nException thrown: "+text+"\n\nSee logs for more information.\nContact enigma@enigmablade.net if the problem continues.", "Am sad :(", JOptionPane.OK_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE, null, options, options[1]) == JOptionPane.OK_OPTION)
				ClipboardUtil.setClipboardContents(text);
			SystemUtil.exit(1);
		}
	}
}
