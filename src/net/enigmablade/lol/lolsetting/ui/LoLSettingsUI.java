package net.enigmablade.lol.lolsetting.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.*;
import javax.swing.event.*;
import net.enigmablade.paradoxion.util.*;
import net.enigmablade.lol.lolsetting.*;
import net.enigmablade.lol.lolsetting.ui.renderers.*;


public class LoLSettingsUI extends JFrame
{
	//Components
	private JButton saveButton;
	private JButton resetButton;
	private JLabel statusLabel;
	
	private JComboBox<DisplayMode> video_resolutionComboBox;
	private ResolutionListCellRenderer video_resolutionRenderer;
	private JComboBox<String> video_windowModeComboBox;
	private JCheckBox video_verticalSyncCheckBox;
	private JComboBox<String> video_framerateComboBox;
	private JTextPane video_framerateTip;
	private JComboBox<String> video_presetsComboBox;
	private JLabel video_framerateLabel;
	private JLabel video_characterQualityLabel;
	private JLabel video_environmentQualityLabel;
	private JLabel video_effectsQualityLabel;
	private JLabel video_shadowQualityLabel;
	private JRadioButton[] video_characterQualityRadioButtons;
	private JRadioButton[] video_environmentQualityRadioButtons;
	private JRadioButton[] video_effectsQualityRadioButtons;
	private JRadioButton[] video_shadowQualityRadioButtons;
	private ButtonGroup video_characterQualityButtonGroup = new ButtonGroup();
	private ButtonGroup video_environmentQualityButtonGroup = new ButtonGroup();
	private ButtonGroup video_effectsQualityButtonGroup = new ButtonGroup();
	private ButtonGroup video_shadowQualityButtonGroup = new ButtonGroup();
	private JCheckBox video_advancedLightingCheckBox;
	private JCheckBox video_advancedReflectionsCheckBox;
	private JCheckBox video_particleOptimizationsCheckBox;
	
	private JButton audio_masterVolumeReset;
	private JButton audio_musicVolumeReset;
	private JButton audio_sfxVolumeReset;
	private JButton audio_voiceVolumeReset;
	private JButton audio_announcerVolumeReset;
	private JCheckBox audio_muteAllCheckBox;
	private JSlider audio_masterVolumeSlider;
	private JSlider audio_musicVolumeSlider;
	private JSlider audio_sfxVolumeSlider;
	private JSlider audio_voiceVolumeSlider;
	private JSlider audio_announcerVolumeSlider;
	private JLabel audio_masterVolumeLabel;
	private JLabel audio_musicVolumeLabel;
	private JLabel audio_soundFXVolumeLabel;
	private JLabel audio_voiceVolumeLabel;
	private JLabel audio_announcerVolumeLabel;
	private JLabel audio_masterVolumeValue;
	private JLabel audio_musicVolumeValue;
	private JLabel audio_sfxVolumeValue;
	private JLabel audio_voiceVolumeValue;
	private JLabel audio_announcerVolumeValue;
	
	private JCheckBox int_oldHudCheckBox;
	private JComboBox<String> int_cooldownComboBox;
	private JButton int_mapScaleReset;
	private JButton int_hudScaleReset;
	private JButton int_chatScaleReset;
	private JCheckBox int_showHPCheckBox;
	private JCheckBox int_showTipsCheckBox;
	private JCheckBox int_hideWallCheckBox;
	private JCheckBox int_hudAnimations;
	private JSlider int_hudScaleSlider;
	private JSlider int_mapScaleSlider;
	private JCheckBox int_flipMinimapCheckBox;
	private JCheckBox int_showNeutralsCheckBox;
	private JSlider int_chatScaleSlider;
	private JCheckBox int_showTimestampsCheckBox;
	private JCheckBox int_showAllChatCheckBox;
	private JRadioButton int_shopDisplayModeList, int_shopDisplayModeGrid;
	private JRadioButton int_shopStartPanelRecommended, int_shopStartPanelAll;
	private ButtonGroup int_shopDisplayModeButtonGroup = new ButtonGroup();
	private ButtonGroup int_shopStartPaneButtonGroup = new ButtonGroup();
	
	private JCheckBox game_colorblindCheckBox;
	private JCheckBox game_damageFlashCheckBox;
	private JCheckBox game_movementPredictCheckBox;
	private JCheckBox game_autoDisplayTargetCheckBox;
	private JCheckBox game_lineMissileDisplayCheckBox;
	private JCheckBox game_smartcastRangeIndicatorsCheckBox;
	private JSlider game_scrollSpeedSlider;
	private JLabel game_scrollSpeedValue;
	private JCheckBox game_cameraSmoothingCheckBox;
	
	private JCheckBox float_legacy;
	private JCheckBox float_damage;
	private JCheckBox float_enemyDamage;
	private JCheckBox float_crit;
	private JCheckBox float_enemyCrit;
	private JCheckBox float_heal;
	private JCheckBox float_dodge;
	private JCheckBox float_gold;
	private JCheckBox float_allyGold;
	private JCheckBox float_level;
	private JCheckBox float_status;
	private JCheckBox float_special;
	private JCheckBox float_quest;
	private JCheckBox float_score;
	private JCheckBox float_mana;
	private JCheckBox float_experience;
	
	//Data
	private LoLSettings main;
	private String titleBase;
	private String version;
	
	private boolean colorblindEnabled = false;
	private JCheckBox game_disableCameraSnap;
	
	public LoLSettingsUI(LoLSettings main, String titleBase, String version, Preferences prefs)
	{
		this.main = main;
		this.titleBase = titleBase;
		this.version = version;
		
		initComponents();
		initMenuBar(prefs);
	}
	
	private void initComponents()
	{
		setTitle("Enigma's Settings Modifier");
		setIconImage(new ImageIcon(getClass().getClassLoader().getResource("resources/images/icon.png")).getImage());
		setBounds(0, 0, 590, 380);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent evt)
			{
				main.gui_close();
				dispose();
				SystemUtil.exit(0);
			}
		});
		
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(0, 2, 2, 2));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel mainButtonPanel = new JPanel();
		mainButtonPanel.setBorder(new EmptyBorder(0, 2, 0, 0));
		contentPane.add(mainButtonPanel, BorderLayout.SOUTH);
		mainButtonPanel.setLayout(new BorderLayout(0, 0));
		
		JPanel sideButtonPanel = new JPanel();
		mainButtonPanel.add(sideButtonPanel, BorderLayout.EAST);
		sideButtonPanel.setLayout(new BorderLayout(0, 0));
		
		saveButton = new JButton("Save");
		saveButton.setMnemonic('s');
		saveButton.setFocusPainted(false);
		sideButtonPanel.add(saveButton);
		saveButton.setPreferredSize(new Dimension(100, 23));
		saveButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				main.gui_save();
			}
		});
		
		resetButton = new JButton("Reset");
		resetButton.setMnemonic('r');
		resetButton.setFocusPainted(false);
		sideButtonPanel.add(resetButton, BorderLayout.EAST);
		resetButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				main.gui_reload();
			}
		});
		
		statusLabel = new JLabel("");
		statusLabel.setBorder(new EmptyBorder(0, 2, 0, 0));
		statusLabel.setFont(statusLabel.getFont().deriveFont(statusLabel.getFont().getSize() + 1f));
		statusLabel.setForeground(Color.RED);
		mainButtonPanel.add(statusLabel, BorderLayout.CENTER);
		
		JTabbedPane mainTabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(mainTabbedPane, BorderLayout.CENTER);
		
		JPanel videoPanel = new JPanel();
		videoPanel.setBorder(new EmptyBorder(1, 2, 0, 2));
		mainTabbedPane.addTab("Video Settings", new ImageIcon(LoLSettingsUI.class.getResource("/resources/images/video_settings.png")), videoPanel, null);
		videoPanel.setLayout(new BorderLayout(0, 0));
		
		JPanel video_alignWPanel = new JPanel();
		videoPanel.add(video_alignWPanel, BorderLayout.WEST);
		video_alignWPanel.setLayout(new BorderLayout(0, 2));
		
		JLabel video_titleLabel = new JLabel("Video Settings");
		video_titleLabel.setFont(video_titleLabel.getFont().deriveFont(video_titleLabel.getFont().getStyle() | Font.BOLD, video_titleLabel.getFont().getSize() + 4f));
		video_alignWPanel.add(video_titleLabel, BorderLayout.NORTH);
		
		JPanel video_displaySettingsPanel = new JPanel();
		video_alignWPanel.add(video_displaySettingsPanel);
		video_displaySettingsPanel.setPreferredSize(new Dimension(250, 0));
		video_displaySettingsPanel.setBorder(new CompoundBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Display Settings", TitledBorder.LEADING, TitledBorder.TOP, null, null), new EmptyBorder(2, 2, 2, 2)));
		GridBagLayout gbl_video_displaySettingsPanel = new GridBagLayout();
		gbl_video_displaySettingsPanel.columnWidths = new int[]{0, 0, 0};
		gbl_video_displaySettingsPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_video_displaySettingsPanel.columnWeights = new double[]{0.0, 0.0, 1.0};
		gbl_video_displaySettingsPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		video_displaySettingsPanel.setLayout(gbl_video_displaySettingsPanel);
		
		JLabel video_resolutionLabel = new JLabel("Resolution:");
		GridBagConstraints gbc_video_resolutionLabel = new GridBagConstraints();
		gbc_video_resolutionLabel.insets = new Insets(0, 0, 4, 2);
		gbc_video_resolutionLabel.anchor = GridBagConstraints.EAST;
		gbc_video_resolutionLabel.gridx = 0;
		gbc_video_resolutionLabel.gridy = 0;
		video_displaySettingsPanel.add(video_resolutionLabel, gbc_video_resolutionLabel);
		
		video_resolutionComboBox = new JComboBox<DisplayMode>();
		video_resolutionComboBox.setModel(new DefaultComboBoxModel<DisplayMode>());
		video_resolutionComboBox.setRenderer(video_resolutionRenderer = new ResolutionListCellRenderer());
		GridBagConstraints gbc_video_resolutionComboBox = new GridBagConstraints();
		gbc_video_resolutionComboBox.gridwidth = 2;
		gbc_video_resolutionComboBox.insets = new Insets(0, 0, 4, 0);
		gbc_video_resolutionComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_video_resolutionComboBox.gridx = 1;
		gbc_video_resolutionComboBox.gridy = 0;
		video_displaySettingsPanel.add(video_resolutionComboBox, gbc_video_resolutionComboBox);
		
		JLabel video_windowModeLabel = new JLabel("Window mode:");
		GridBagConstraints gbc_video_windowModeLabel = new GridBagConstraints();
		gbc_video_windowModeLabel.anchor = GridBagConstraints.EAST;
		gbc_video_windowModeLabel.insets = new Insets(0, 0, 8, 2);
		gbc_video_windowModeLabel.gridx = 0;
		gbc_video_windowModeLabel.gridy = 1;
		video_displaySettingsPanel.add(video_windowModeLabel, gbc_video_windowModeLabel);
		
		video_windowModeComboBox = new JComboBox<String>();
		video_windowModeComboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"Fullscreen", "Windowed", "Borderless Windowed"}));
		video_windowModeComboBox.setRenderer(new BasicStripedListCellRenderer());
		GridBagConstraints gbc_video_windowModeComboBox = new GridBagConstraints();
		gbc_video_windowModeComboBox.gridwidth = 2;
		gbc_video_windowModeComboBox.insets = new Insets(0, 0, 8, 0);
		gbc_video_windowModeComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_video_windowModeComboBox.gridx = 1;
		gbc_video_windowModeComboBox.gridy = 1;
		video_displaySettingsPanel.add(video_windowModeComboBox, gbc_video_windowModeComboBox);
		
		video_verticalSyncCheckBox = new JCheckBox("Vertical Sync");
		video_verticalSyncCheckBox.setFocusPainted(false);
		video_verticalSyncCheckBox.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent evt)
			{
				boolean enabled = evt.getStateChange() == ItemEvent.DESELECTED;
				video_framerateComboBox.setEnabled(enabled);
				video_framerateLabel.setEnabled(enabled);
			}
		});
		GridBagConstraints gbc_video_verticalSyncCheckBox = new GridBagConstraints();
		gbc_video_verticalSyncCheckBox.gridwidth = 2;
		gbc_video_verticalSyncCheckBox.anchor = GridBagConstraints.WEST;
		gbc_video_verticalSyncCheckBox.gridx = 0;
		gbc_video_verticalSyncCheckBox.gridy = 2;
		video_displaySettingsPanel.add(video_verticalSyncCheckBox, gbc_video_verticalSyncCheckBox);
		
		JTextPane video_verticalSyncTip = new JTextPane();
		video_verticalSyncTip.setEnabled(false);
		video_verticalSyncTip.setEditable(false);
		video_verticalSyncTip.setText("Enabling Vertical Sync locks the framerate of the game to the refresh rate of your monitor.\r\nEnable if you experience vertical tearing, but not if you're worried about performance.");
		GridBagConstraints gbc_video_verticalSyncTip = new GridBagConstraints();
		gbc_video_verticalSyncTip.insets = new Insets(0, 0, 4, 0);
		gbc_video_verticalSyncTip.gridwidth = 3;
		gbc_video_verticalSyncTip.fill = GridBagConstraints.BOTH;
		gbc_video_verticalSyncTip.gridx = 0;
		gbc_video_verticalSyncTip.gridy = 3;
		video_displaySettingsPanel.add(video_verticalSyncTip, gbc_video_verticalSyncTip);
		
		video_framerateLabel = new JLabel("Framerate cap (max FPS):");
		GridBagConstraints gbc_video_framerateLabel = new GridBagConstraints();
		gbc_video_framerateLabel.insets = new Insets(0, 0, 0, 2);
		gbc_video_framerateLabel.anchor = GridBagConstraints.EAST;
		gbc_video_framerateLabel.gridwidth = 2;
		gbc_video_framerateLabel.gridx = 0;
		gbc_video_framerateLabel.gridy = 4;
		video_displaySettingsPanel.add(video_framerateLabel, gbc_video_framerateLabel);
		
		video_framerateComboBox = new JComboBox<String>();
		video_framerateComboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"Stable", "High", "Benchmark", "25 FPS", "30 FPS", "60 FPS", "80 FPS"}));
		video_framerateComboBox.setRenderer(new BasicStripedListCellRenderer());
		video_framerateComboBox.addItemListener(new ItemListener(){
			@SuppressWarnings("unchecked")
			@Override
			public void itemStateChanged(ItemEvent evt)
			{
				if(evt.getStateChange() == ItemEvent.SELECTED)
				{
					String text;
					switch(((JComboBox<String>)evt.getSource()).getSelectedIndex())
					{
						//Stable
						case 0: text = "Your framerate will be controlled to prevent visual stuttering.";
							break;
						//High
						case 1: text = "";
							break;
						//Benchmark
						case 2: text = "Your framerate will not be capped (max 100 FPS), but overheating or visual stuttering may occur.";
							break;
						
						default: text = "";
					}
					video_framerateTip.setText(text);
				}
			}
		});
		video_framerateComboBox.setSelectedIndex(-1);
		GridBagConstraints gbc_video_framerateComboBox = new GridBagConstraints();
		gbc_video_framerateComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_video_framerateComboBox.gridx = 2;
		gbc_video_framerateComboBox.gridy = 4;
		video_displaySettingsPanel.add(video_framerateComboBox, gbc_video_framerateComboBox);
		
		video_framerateTip = new JTextPane();
		video_framerateTip.setEnabled(false);
		video_framerateTip.setEditable(false);
		GridBagConstraints gbc_video_framerateTip = new GridBagConstraints();
		gbc_video_framerateTip.gridwidth = 3;
		gbc_video_framerateTip.fill = GridBagConstraints.BOTH;
		gbc_video_framerateTip.gridx = 0;
		gbc_video_framerateTip.gridy = 5;
		video_displaySettingsPanel.add(video_framerateTip, gbc_video_framerateTip);
		
		JPanel video_alignEPanel = new JPanel();
		video_alignEPanel.setPreferredSize(new Dimension(310, 0));
		video_alignEPanel.setBorder(new CompoundBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Graphics Settings", TitledBorder.LEADING, TitledBorder.TOP, null, null), new EmptyBorder(2, 2, 2, 2)));
		videoPanel.add(video_alignEPanel);
		GridBagLayout gbl_video_alignEPanel = new GridBagLayout();
		gbl_video_alignEPanel.columnWidths = new int[]{0, 0, 0, -19, 27, 0};
		gbl_video_alignEPanel.rowHeights = new int[]{0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_video_alignEPanel.columnWeights = new double[]{1.0, 1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_video_alignEPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		video_alignEPanel.setLayout(gbl_video_alignEPanel);
		
		JLabel video_presetsLabel = new JLabel("Presets:");
		GridBagConstraints gbc_video_presetsLabel = new GridBagConstraints();
		gbc_video_presetsLabel.insets = new Insets(0, 2, 6, 2);
		gbc_video_presetsLabel.gridx = 0;
		gbc_video_presetsLabel.gridy = 0;
		video_alignEPanel.add(video_presetsLabel, gbc_video_presetsLabel);
		
		video_presetsComboBox = new JComboBox<String>();
		video_presetsComboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"Auto", "Very Low", "Low", "Medium", "High", "Very High", "Custom"}));
		video_presetsComboBox.setRenderer(new BasicStripedListCellRenderer());
		video_presetsComboBox.setSelectedIndex(0);
		video_presetsComboBox.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent evt)
			{
				@SuppressWarnings("unchecked")
				JComboBox<String> source = (JComboBox<String>)evt.getSource();
				int index = source.getSelectedIndex();
				
				if(evt.getStateChange() == ItemEvent.SELECTED)
				{
					boolean enable = index != 0;
					video_characterQualityLabel.setEnabled(enable);
					video_environmentQualityLabel.setEnabled(enable);
					video_effectsQualityLabel.setEnabled(enable);
					video_shadowQualityLabel.setEnabled(enable);
					video_advancedLightingCheckBox.setEnabled(enable);
					video_advancedReflectionsCheckBox.setEnabled(enable);
					video_particleOptimizationsCheckBox.setEnabled(enable);
					for(int n = 0; n < video_characterQualityRadioButtons.length; n++)
					{
						video_characterQualityRadioButtons[n].setEnabled(enable);
						video_environmentQualityRadioButtons[n].setEnabled(enable);
						video_effectsQualityRadioButtons[n].setEnabled(enable);
						video_shadowQualityRadioButtons[n].setEnabled(enable);
					}
					
					if(index == 0)
					{
						//video_characterQualityButtonGroup.clearSelection();
						//video_environmentQualityButtonGroup.clearSelection();
						//video_effectQualityButtonGroup.clearSelection();
						//video_shadowQualityButtonGroup.clearSelection();
						//video_advancedLightingCheckBox.setSelected(false);
						//video_advancedReflectionsCheckBox.setSelected(false);
					}
					else if(index < source.getItemCount()-1)
					{
						index--;
						video_characterQualityRadioButtons[index].setSelected(true);
						video_environmentQualityRadioButtons[index].setSelected(true);
						video_effectsQualityRadioButtons[index].setSelected(true);
						video_shadowQualityRadioButtons[index].setSelected(true);
						video_advancedLightingCheckBox.setSelected(index >= 2);
						video_advancedReflectionsCheckBox.setSelected(index >= 2);
						video_particleOptimizationsCheckBox.setSelected(index < 2);
					}
				}
			}
		});
		GridBagConstraints gbc_video_presetsComboBox = new GridBagConstraints();
		gbc_video_presetsComboBox.insets = new Insets(0, 0, 6, 0);
		gbc_video_presetsComboBox.gridwidth = 2;
		gbc_video_presetsComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_video_presetsComboBox.gridx = 1;
		gbc_video_presetsComboBox.gridy = 0;
		video_alignEPanel.add(video_presetsComboBox, gbc_video_presetsComboBox);
		
		JSeparator video_graphicsSeparator = new JSeparator();
		GridBagConstraints gbc_video_graphicsSeparator = new GridBagConstraints();
		gbc_video_graphicsSeparator.gridwidth = 5;
		gbc_video_graphicsSeparator.insets = new Insets(0, 0, 4, 0);
		gbc_video_graphicsSeparator.fill = GridBagConstraints.HORIZONTAL;
		gbc_video_graphicsSeparator.gridx = 0;
		gbc_video_graphicsSeparator.gridy = 1;
		video_alignEPanel.add(video_graphicsSeparator, gbc_video_graphicsSeparator);
		
		MouseListener qualityChangeListener = new MouseAdapter(){
			@Override
			public void mousePressed(MouseEvent evt)
			{
				JRadioButton button = (JRadioButton)evt.getSource();
				if(button.isEnabled())
					video_presetsComboBox.setSelectedIndex(video_presetsComboBox.getItemCount()-1);
			}
		};
		
		video_characterQualityLabel = new JLabel("Character Quality:");
		video_characterQualityLabel.setEnabled(false);
		GridBagConstraints gbc_video_characterQualityLabel = new GridBagConstraints();
		gbc_video_characterQualityLabel.anchor = GridBagConstraints.WEST;
		gbc_video_characterQualityLabel.gridwidth = 2;
		gbc_video_characterQualityLabel.gridx = 0;
		gbc_video_characterQualityLabel.gridy = 2;
		video_alignEPanel.add(video_characterQualityLabel, gbc_video_characterQualityLabel);
		
		JRadioButton video_characterQuality1 = new JRadioButton("Very Low");
		video_characterQuality1.addMouseListener(qualityChangeListener);
		video_characterQuality1.setEnabled(false);
		video_characterQualityButtonGroup.add(video_characterQuality1);
		video_characterQuality1.setFocusPainted(false);
		GridBagConstraints gbc_video_characterQuality1 = new GridBagConstraints();
		gbc_video_characterQuality1.anchor = GridBagConstraints.WEST;
		gbc_video_characterQuality1.gridx = 0;
		gbc_video_characterQuality1.gridy = 3;
		video_alignEPanel.add(video_characterQuality1, gbc_video_characterQuality1);
		
		JRadioButton video_characterQuality2 = new JRadioButton("Low");
		video_characterQuality2.addMouseListener(qualityChangeListener);
		video_characterQuality2.setEnabled(false);
		video_characterQualityButtonGroup.add(video_characterQuality2);
		video_characterQuality2.setFocusPainted(false);
		GridBagConstraints gbc_video_characterQuality2 = new GridBagConstraints();
		gbc_video_characterQuality2.anchor = GridBagConstraints.WEST;
		gbc_video_characterQuality2.gridx = 1;
		gbc_video_characterQuality2.gridy = 3;
		video_alignEPanel.add(video_characterQuality2, gbc_video_characterQuality2);
		
		JRadioButton video_characterQuality3 = new JRadioButton("Medium");
		video_characterQuality3.addMouseListener(qualityChangeListener);
		video_characterQuality3.setEnabled(false);
		video_characterQualityButtonGroup.add(video_characterQuality3);
		video_characterQuality3.setFocusPainted(false);
		GridBagConstraints gbc_video_characterQuality3 = new GridBagConstraints();
		gbc_video_characterQuality3.anchor = GridBagConstraints.WEST;
		gbc_video_characterQuality3.gridx = 2;
		gbc_video_characterQuality3.gridy = 3;
		video_alignEPanel.add(video_characterQuality3, gbc_video_characterQuality3);
		
		JRadioButton video_characterQuality4 = new JRadioButton("High");
		video_characterQuality4.addMouseListener(qualityChangeListener);
		video_characterQuality4.setEnabled(false);
		video_characterQualityButtonGroup.add(video_characterQuality4);
		video_characterQuality4.setFocusPainted(false);
		GridBagConstraints gbc_video_characterQuality4 = new GridBagConstraints();
		gbc_video_characterQuality4.anchor = GridBagConstraints.WEST;
		gbc_video_characterQuality4.gridx = 3;
		gbc_video_characterQuality4.gridy = 3;
		video_alignEPanel.add(video_characterQuality4, gbc_video_characterQuality4);
		
		JRadioButton video_characterQuality5 = new JRadioButton("Very High");
		video_characterQuality5.addMouseListener(qualityChangeListener);
		video_characterQuality5.setEnabled(false);
		video_characterQualityButtonGroup.add(video_characterQuality5);
		video_characterQuality5.setFocusPainted(false);
		GridBagConstraints gbc_video_characterQuality5 = new GridBagConstraints();
		gbc_video_characterQuality5.anchor = GridBagConstraints.WEST;
		gbc_video_characterQuality5.gridx = 4;
		gbc_video_characterQuality5.gridy = 3;
		video_alignEPanel.add(video_characterQuality5, gbc_video_characterQuality5);
		
		video_characterQualityRadioButtons = new JRadioButton[5];
		video_characterQualityRadioButtons[0] = video_characterQuality1;
		video_characterQualityRadioButtons[1] = video_characterQuality2;
		video_characterQualityRadioButtons[2] = video_characterQuality3;
		video_characterQualityRadioButtons[3] = video_characterQuality4;
		video_characterQualityRadioButtons[4] = video_characterQuality5;
		
		video_environmentQualityLabel = new JLabel("Environment Quality:");
		video_environmentQualityLabel.setEnabled(false);
		GridBagConstraints gbc_video_environmentQualityLabel = new GridBagConstraints();
		gbc_video_environmentQualityLabel.insets = new Insets(4, 0, 0, 0);
		gbc_video_environmentQualityLabel.anchor = GridBagConstraints.WEST;
		gbc_video_environmentQualityLabel.gridwidth = 2;
		gbc_video_environmentQualityLabel.gridx = 0;
		gbc_video_environmentQualityLabel.gridy = 4;
		video_alignEPanel.add(video_environmentQualityLabel, gbc_video_environmentQualityLabel);
		
		JRadioButton video_environmentQuality1 = new JRadioButton("Very Low");
		video_environmentQuality1.addMouseListener(qualityChangeListener);
		video_environmentQuality1.setEnabled(false);
		video_environmentQualityButtonGroup.add(video_environmentQuality1);
		video_environmentQuality1.setFocusPainted(false);
		GridBagConstraints gbc_video_environmentQuality1 = new GridBagConstraints();
		gbc_video_environmentQuality1.anchor = GridBagConstraints.WEST;
		gbc_video_environmentQuality1.gridx = 0;
		gbc_video_environmentQuality1.gridy = 5;
		video_alignEPanel.add(video_environmentQuality1, gbc_video_environmentQuality1);
		
		JRadioButton video_environmentQuality2 = new JRadioButton("Low");
		video_environmentQuality2.addMouseListener(qualityChangeListener);
		video_environmentQuality2.setEnabled(false);
		video_environmentQualityButtonGroup.add(video_environmentQuality2);
		video_environmentQuality2.setFocusPainted(false);
		GridBagConstraints gbc_video_environmentQuality2 = new GridBagConstraints();
		gbc_video_environmentQuality2.anchor = GridBagConstraints.WEST;
		gbc_video_environmentQuality2.gridx = 1;
		gbc_video_environmentQuality2.gridy = 5;
		video_alignEPanel.add(video_environmentQuality2, gbc_video_environmentQuality2);
		
		JRadioButton video_environmentQuality3 = new JRadioButton("Medium");
		video_environmentQuality3.addMouseListener(qualityChangeListener);
		video_environmentQuality3.setEnabled(false);
		video_environmentQualityButtonGroup.add(video_environmentQuality3);
		video_environmentQuality3.setFocusPainted(false);
		GridBagConstraints gbc_video_environmentQuality3 = new GridBagConstraints();
		gbc_video_environmentQuality3.anchor = GridBagConstraints.WEST;
		gbc_video_environmentQuality3.gridx = 2;
		gbc_video_environmentQuality3.gridy = 5;
		video_alignEPanel.add(video_environmentQuality3, gbc_video_environmentQuality3);
		
		JRadioButton video_environmentQuality4 = new JRadioButton("High");
		video_environmentQuality4.addMouseListener(qualityChangeListener);
		video_environmentQuality4.setEnabled(false);
		video_environmentQualityButtonGroup.add(video_environmentQuality4);
		video_environmentQuality4.setFocusPainted(false);
		GridBagConstraints gbc_video_environmentQuality4 = new GridBagConstraints();
		gbc_video_environmentQuality4.anchor = GridBagConstraints.WEST;
		gbc_video_environmentQuality4.gridx = 3;
		gbc_video_environmentQuality4.gridy = 5;
		video_alignEPanel.add(video_environmentQuality4, gbc_video_environmentQuality4);
		
		JRadioButton video_environmentQuality5 = new JRadioButton("Very High");
		video_environmentQuality5.addMouseListener(qualityChangeListener);
		video_environmentQuality5.setEnabled(false);
		video_environmentQualityButtonGroup.add(video_environmentQuality5);
		video_environmentQuality5.setFocusPainted(false);
		GridBagConstraints gbc_video_environmentQuality5 = new GridBagConstraints();
		gbc_video_environmentQuality5.anchor = GridBagConstraints.WEST;
		gbc_video_environmentQuality5.gridx = 4;
		gbc_video_environmentQuality5.gridy = 5;
		video_alignEPanel.add(video_environmentQuality5, gbc_video_environmentQuality5);
		
		video_environmentQualityRadioButtons = new JRadioButton[5];
		video_environmentQualityRadioButtons[0] = video_environmentQuality1;
		video_environmentQualityRadioButtons[1] = video_environmentQuality2;
		video_environmentQualityRadioButtons[2] = video_environmentQuality3;
		video_environmentQualityRadioButtons[3] = video_environmentQuality4;
		video_environmentQualityRadioButtons[4] = video_environmentQuality5;
		
		video_effectsQualityLabel = new JLabel("Effects Quality:");
		video_effectsQualityLabel.setEnabled(false);
		GridBagConstraints gbc_video_effectsQualityLabel = new GridBagConstraints();
		gbc_video_effectsQualityLabel.insets = new Insets(4, 0, 0, 0);
		gbc_video_effectsQualityLabel.anchor = GridBagConstraints.WEST;
		gbc_video_effectsQualityLabel.gridwidth = 2;
		gbc_video_effectsQualityLabel.gridx = 0;
		gbc_video_effectsQualityLabel.gridy = 6;
		video_alignEPanel.add(video_effectsQualityLabel, gbc_video_effectsQualityLabel);
		
		JRadioButton video_effectsQuality1 = new JRadioButton("Very Low");
		video_effectsQuality1.addMouseListener(qualityChangeListener);
		video_effectsQuality1.setEnabled(false);
		video_effectsQualityButtonGroup.add(video_effectsQuality1);
		video_effectsQuality1.setFocusPainted(false);
		GridBagConstraints gbc_video_effectsQuality1 = new GridBagConstraints();
		gbc_video_effectsQuality1.anchor = GridBagConstraints.WEST;
		gbc_video_effectsQuality1.gridx = 0;
		gbc_video_effectsQuality1.gridy = 7;
		video_alignEPanel.add(video_effectsQuality1, gbc_video_effectsQuality1);
		
		JRadioButton video_effectsQuality2 = new JRadioButton("Low");
		video_effectsQuality2.addMouseListener(qualityChangeListener);
		video_effectsQuality2.setEnabled(false);
		video_effectsQualityButtonGroup.add(video_effectsQuality2);
		video_effectsQuality2.setFocusPainted(false);
		GridBagConstraints gbc_video_effectsQuality2 = new GridBagConstraints();
		gbc_video_effectsQuality2.anchor = GridBagConstraints.WEST;
		gbc_video_effectsQuality2.gridx = 1;
		gbc_video_effectsQuality2.gridy = 7;
		video_alignEPanel.add(video_effectsQuality2, gbc_video_effectsQuality2);
		
		JRadioButton video_effectsQuality3 = new JRadioButton("Medium");
		video_effectsQuality3.addMouseListener(qualityChangeListener);
		video_effectsQuality3.setEnabled(false);
		video_effectsQualityButtonGroup.add(video_effectsQuality3);
		video_effectsQuality3.setFocusPainted(false);
		GridBagConstraints gbc_video_effectsQuality3 = new GridBagConstraints();
		gbc_video_effectsQuality3.anchor = GridBagConstraints.WEST;
		gbc_video_effectsQuality3.gridx = 2;
		gbc_video_effectsQuality3.gridy = 7;
		video_alignEPanel.add(video_effectsQuality3, gbc_video_effectsQuality3);
		
		JRadioButton video_effectsQuality4 = new JRadioButton("High");
		video_effectsQuality4.addMouseListener(qualityChangeListener);
		video_effectsQuality4.setEnabled(false);
		video_effectsQualityButtonGroup.add(video_effectsQuality4);
		video_effectsQuality4.setFocusPainted(false);
		GridBagConstraints gbc_video_effectsQuality4 = new GridBagConstraints();
		gbc_video_effectsQuality4.anchor = GridBagConstraints.WEST;
		gbc_video_effectsQuality4.gridx = 3;
		gbc_video_effectsQuality4.gridy = 7;
		video_alignEPanel.add(video_effectsQuality4, gbc_video_effectsQuality4);
		
		JRadioButton video_effectsQuality5 = new JRadioButton("Very High");
		video_effectsQuality5.addMouseListener(qualityChangeListener);
		video_effectsQuality5.setEnabled(false);
		video_effectsQualityButtonGroup.add(video_effectsQuality5);
		video_effectsQuality5.setFocusPainted(false);
		GridBagConstraints gbc_video_effectsQuality5 = new GridBagConstraints();
		gbc_video_effectsQuality5.anchor = GridBagConstraints.WEST;
		gbc_video_effectsQuality5.gridx = 4;
		gbc_video_effectsQuality5.gridy = 7;
		video_alignEPanel.add(video_effectsQuality5, gbc_video_effectsQuality5);
		
		video_effectsQualityRadioButtons = new JRadioButton[5];
		video_effectsQualityRadioButtons[0] = video_effectsQuality1;
		video_effectsQualityRadioButtons[1] = video_effectsQuality2;
		video_effectsQualityRadioButtons[2] = video_effectsQuality3;
		video_effectsQualityRadioButtons[3] = video_effectsQuality4;
		video_effectsQualityRadioButtons[4] = video_effectsQuality5;
		
		video_shadowQualityLabel = new JLabel("Shadows:");
		video_shadowQualityLabel.setEnabled(false);
		GridBagConstraints gbc_video_shadowQualityLabel = new GridBagConstraints();
		gbc_video_shadowQualityLabel.insets = new Insets(4, 0, 0, 0);
		gbc_video_shadowQualityLabel.anchor = GridBagConstraints.WEST;
		gbc_video_shadowQualityLabel.gridwidth = 2;
		gbc_video_shadowQualityLabel.gridx = 0;
		gbc_video_shadowQualityLabel.gridy = 8;
		video_alignEPanel.add(video_shadowQualityLabel, gbc_video_shadowQualityLabel);
		
		JRadioButton video_shadowQuality1 = new JRadioButton("None");
		video_shadowQuality1.addMouseListener(qualityChangeListener);
		video_shadowQuality1.setEnabled(false);
		video_shadowQuality1.setToolTipText("The perfect Zed counter");
		video_shadowQualityButtonGroup.add(video_shadowQuality1);
		video_shadowQuality1.setFocusPainted(false);
		GridBagConstraints gbc_video_shadowQuality1 = new GridBagConstraints();
		gbc_video_shadowQuality1.anchor = GridBagConstraints.WEST;
		gbc_video_shadowQuality1.gridx = 0;
		gbc_video_shadowQuality1.gridy = 9;
		video_alignEPanel.add(video_shadowQuality1, gbc_video_shadowQuality1);
		
		JRadioButton video_shadowQuality2 = new JRadioButton("Low");
		video_shadowQuality2.addMouseListener(qualityChangeListener);
		video_shadowQuality2.setEnabled(false);
		video_shadowQualityButtonGroup.add(video_shadowQuality2);
		video_shadowQuality2.setFocusPainted(false);
		GridBagConstraints gbc_video_shadowQuality2 = new GridBagConstraints();
		gbc_video_shadowQuality2.anchor = GridBagConstraints.WEST;
		gbc_video_shadowQuality2.gridx = 1;
		gbc_video_shadowQuality2.gridy = 9;
		video_alignEPanel.add(video_shadowQuality2, gbc_video_shadowQuality2);
		
		JRadioButton video_shadowQuality3 = new JRadioButton("Medium");
		video_shadowQuality3.addMouseListener(qualityChangeListener);
		video_shadowQuality3.setEnabled(false);
		video_shadowQualityButtonGroup.add(video_shadowQuality3);
		video_shadowQuality3.setFocusPainted(false);
		GridBagConstraints gbc_video_shadowQuality3 = new GridBagConstraints();
		gbc_video_shadowQuality3.anchor = GridBagConstraints.WEST;
		gbc_video_shadowQuality3.gridx = 2;
		gbc_video_shadowQuality3.gridy = 9;
		video_alignEPanel.add(video_shadowQuality3, gbc_video_shadowQuality3);
		
		JRadioButton video_shadowQuality4 = new JRadioButton("High");
		video_shadowQuality4.addMouseListener(qualityChangeListener);
		video_shadowQuality4.setEnabled(false);
		video_shadowQualityButtonGroup.add(video_shadowQuality4);
		video_shadowQuality4.setFocusPainted(false);
		GridBagConstraints gbc_video_shadowQuality4 = new GridBagConstraints();
		gbc_video_shadowQuality4.anchor = GridBagConstraints.WEST;
		gbc_video_shadowQuality4.gridx = 3;
		gbc_video_shadowQuality4.gridy = 9;
		video_alignEPanel.add(video_shadowQuality4, gbc_video_shadowQuality4);
		
		JRadioButton video_shadowQuality5 = new JRadioButton("Very High");
		video_shadowQuality5.addMouseListener(qualityChangeListener);
		video_shadowQuality5.setEnabled(false);
		video_shadowQualityButtonGroup.add(video_shadowQuality5);
		video_shadowQuality5.setFocusPainted(false);
		GridBagConstraints gbc_video_shadowQuality5 = new GridBagConstraints();
		gbc_video_shadowQuality5.anchor = GridBagConstraints.WEST;
		gbc_video_shadowQuality5.gridx = 4;
		gbc_video_shadowQuality5.gridy = 9;
		video_alignEPanel.add(video_shadowQuality5, gbc_video_shadowQuality5);
		
		video_shadowQualityRadioButtons = new JRadioButton[5];
		video_shadowQualityRadioButtons[0] = video_shadowQuality1;
		video_shadowQualityRadioButtons[1] = video_shadowQuality2;
		video_shadowQualityRadioButtons[2] = video_shadowQuality3;
		video_shadowQualityRadioButtons[3] = video_shadowQuality4;
		video_shadowQualityRadioButtons[4] = video_shadowQuality5;
		
		video_advancedLightingCheckBox = new JCheckBox("Per-pixel lighting");
		video_advancedLightingCheckBox.setToolTipText("Lighting gets calculated on a per-pixel basis. Disabling it may improve performance.");
		video_advancedLightingCheckBox.setEnabled(false);
		video_advancedLightingCheckBox.setFocusPainted(false);
		GridBagConstraints gbc_video_advancedLightingCheckBox = new GridBagConstraints();
		gbc_video_advancedLightingCheckBox.insets = new Insets(4, 0, 0, 0);
		gbc_video_advancedLightingCheckBox.anchor = GridBagConstraints.WEST;
		gbc_video_advancedLightingCheckBox.gridwidth = 2;
		gbc_video_advancedLightingCheckBox.gridx = 0;
		gbc_video_advancedLightingCheckBox.gridy = 10;
		video_alignEPanel.add(video_advancedLightingCheckBox, gbc_video_advancedLightingCheckBox);
		
		video_particleOptimizationsCheckBox = new JCheckBox("Enable particle optimizations");
		video_particleOptimizationsCheckBox.setFocusPainted(false);
		video_particleOptimizationsCheckBox.setEnabled(false);
		GridBagConstraints gbc_video_enableParticleOptimizations = new GridBagConstraints();
		gbc_video_enableParticleOptimizations.insets = new Insets(4, 0, 0, 0);
		gbc_video_enableParticleOptimizations.anchor = GridBagConstraints.EAST;
		gbc_video_enableParticleOptimizations.gridwidth = 3;
		gbc_video_enableParticleOptimizations.gridx = 2;
		gbc_video_enableParticleOptimizations.gridy = 10;
		video_alignEPanel.add(video_particleOptimizationsCheckBox, gbc_video_enableParticleOptimizations);
		
		video_advancedReflectionsCheckBox = new JCheckBox("Advanced reflections");
		video_advancedReflectionsCheckBox.setToolTipText("More advanced and better looking reflections. Disabling it may improve performance.");
		video_advancedReflectionsCheckBox.setFocusPainted(false);
		video_advancedReflectionsCheckBox.setEnabled(false);
		GridBagConstraints gbc_video_advancedReflectionsCheckBox = new GridBagConstraints();
		gbc_video_advancedReflectionsCheckBox.anchor = GridBagConstraints.WEST;
		gbc_video_advancedReflectionsCheckBox.gridwidth = 3;
		gbc_video_advancedReflectionsCheckBox.gridx = 0;
		gbc_video_advancedReflectionsCheckBox.gridy = 11;
		video_alignEPanel.add(video_advancedReflectionsCheckBox, gbc_video_advancedReflectionsCheckBox);
		
		JPanel audioPanel = new JPanel();
		audioPanel.setBorder(new EmptyBorder(2, 2, 2, 2));
		mainTabbedPane.addTab("Audio Settings", new ImageIcon(LoLSettingsUI.class.getResource("/resources/images/audio_settings.png")), audioPanel, null);
		GridBagLayout gbl_audioPanel = new GridBagLayout();
		gbl_audioPanel.columnWidths = new int[]{0, 0, 0, 30, 0, 0, 0};
		gbl_audioPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_audioPanel.columnWeights = new double[]{1.0, 0.0, 1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_audioPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		audioPanel.setLayout(gbl_audioPanel);
		
		JLabel audio_titleLabel = new JLabel("Audio Settings");
		audio_titleLabel.setFont(audio_titleLabel.getFont().deriveFont(audio_titleLabel.getFont().getStyle() | Font.BOLD, audio_titleLabel.getFont().getSize() + 4f));
		GridBagConstraints gbc_audio_titleLabel = new GridBagConstraints();
		gbc_audio_titleLabel.anchor = GridBagConstraints.WEST;
		gbc_audio_titleLabel.gridwidth = 2;
		gbc_audio_titleLabel.gridx = 0;
		gbc_audio_titleLabel.gridy = 0;
		audioPanel.add(audio_titleLabel, gbc_audio_titleLabel);
		
		audio_muteAllCheckBox = new JCheckBox("Mute all sounds");
		audio_muteAllCheckBox.setFocusPainted(false);
		audio_muteAllCheckBox.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent evt)
			{
				boolean enable = evt.getStateChange() == ItemEvent.DESELECTED;
				audio_masterVolumeLabel.setEnabled(enable);
				audio_masterVolumeSlider.setEnabled(enable);
				audio_masterVolumeValue.setEnabled(enable);
				audio_masterVolumeReset.setEnabled(enable);
				audio_musicVolumeLabel.setEnabled(enable);
				audio_musicVolumeSlider.setEnabled(enable);
				audio_musicVolumeValue.setEnabled(enable);
				audio_musicVolumeReset.setEnabled(enable);
				audio_soundFXVolumeLabel.setEnabled(enable);
				audio_sfxVolumeSlider.setEnabled(enable);
				audio_sfxVolumeValue.setEnabled(enable);
				audio_sfxVolumeReset.setEnabled(enable);
				audio_voiceVolumeLabel.setEnabled(enable);
				audio_voiceVolumeSlider.setEnabled(enable);
				audio_voiceVolumeValue.setEnabled(enable);
				audio_voiceVolumeReset.setEnabled(enable);
				audio_announcerVolumeLabel.setEnabled(enable);
				audio_announcerVolumeSlider.setEnabled(enable);
				audio_announcerVolumeValue.setEnabled(enable);
				audio_announcerVolumeReset.setEnabled(enable);
			}
		});
		GridBagConstraints gbc_audio_muteAllCheckBox = new GridBagConstraints();
		gbc_audio_muteAllCheckBox.insets = new Insets(2, 0, 4, 0);
		gbc_audio_muteAllCheckBox.gridx = 1;
		gbc_audio_muteAllCheckBox.gridy = 1;
		audioPanel.add(audio_muteAllCheckBox, gbc_audio_muteAllCheckBox);
		
		audio_masterVolumeLabel = new JLabel("Master Volume:");
		GridBagConstraints gbc_audio_masterVolumeLabel = new GridBagConstraints();
		gbc_audio_masterVolumeLabel.insets = new Insets(0, 0, 0, 2);
		gbc_audio_masterVolumeLabel.anchor = GridBagConstraints.EAST;
		gbc_audio_masterVolumeLabel.gridx = 1;
		gbc_audio_masterVolumeLabel.gridy = 2;
		audioPanel.add(audio_masterVolumeLabel, gbc_audio_masterVolumeLabel);
		
		audio_masterVolumeSlider = new JSlider();
		audio_masterVolumeSlider.setFocusable(false);
		audio_masterVolumeSlider.setSnapToTicks(true);
		audio_masterVolumeSlider.setMinorTickSpacing(5);
		audio_masterVolumeSlider.setMajorTickSpacing(25);
		audio_masterVolumeSlider.setValue(100);
		audio_masterVolumeSlider.setPaintTicks(true);
		audio_masterVolumeSlider.addChangeListener(new ChangeListener(){
			@Override
			public void stateChanged(ChangeEvent evt)
			{
				JSlider source = (JSlider)evt.getSource();
				audio_masterVolumeValue.setText(source.getValue()+"");
			}
		});
		GridBagConstraints gbc_audio_masterVolumeSlider = new GridBagConstraints();
		gbc_audio_masterVolumeSlider.fill = GridBagConstraints.HORIZONTAL;
		gbc_audio_masterVolumeSlider.gridx = 2;
		gbc_audio_masterVolumeSlider.gridy = 2;
		audioPanel.add(audio_masterVolumeSlider, gbc_audio_masterVolumeSlider);
		
		audio_masterVolumeValue = new JLabel("100");
		GridBagConstraints gbc_audio_masterVolumeValue = new GridBagConstraints();
		gbc_audio_masterVolumeValue.insets = new Insets(0, 0, 0, 2);
		gbc_audio_masterVolumeValue.gridx = 3;
		gbc_audio_masterVolumeValue.gridy = 2;
		audioPanel.add(audio_masterVolumeValue, gbc_audio_masterVolumeValue);
		
		audio_masterVolumeReset = new JButton("Reset");
		audio_masterVolumeReset.setFocusPainted(false);
		audio_masterVolumeReset.setBorder(new EmptyBorder(4, 8, 4, 8));
		audio_masterVolumeReset.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				audio_masterVolumeSlider.setValue(100);
			}
		});
		GridBagConstraints gbc_audio_masterVolumeReset = new GridBagConstraints();
		gbc_audio_masterVolumeReset.gridx = 4;
		gbc_audio_masterVolumeReset.gridy = 2;
		audioPanel.add(audio_masterVolumeReset, gbc_audio_masterVolumeReset);
		
		audio_musicVolumeLabel = new JLabel("Music Volume:");
		GridBagConstraints gbc_audio_musicVolumeLabel = new GridBagConstraints();
		gbc_audio_musicVolumeLabel.anchor = GridBagConstraints.EAST;
		gbc_audio_musicVolumeLabel.insets = new Insets(0, 0, 0, 2);
		gbc_audio_musicVolumeLabel.gridx = 1;
		gbc_audio_musicVolumeLabel.gridy = 3;
		audioPanel.add(audio_musicVolumeLabel, gbc_audio_musicVolumeLabel);
		
		audio_musicVolumeSlider = new JSlider();
		audio_musicVolumeSlider.setFocusable(false);
		audio_musicVolumeSlider.setSnapToTicks(true);
		audio_musicVolumeSlider.setMajorTickSpacing(25);
		audio_musicVolumeSlider.setMinorTickSpacing(5);
		audio_musicVolumeSlider.setPaintTicks(true);
		audio_musicVolumeSlider.addChangeListener(new ChangeListener(){
			@Override
			public void stateChanged(ChangeEvent evt)
			{
				JSlider source = (JSlider)evt.getSource();
				audio_musicVolumeValue.setText(source.getValue()+"");
			}
		});
		GridBagConstraints gbc_audio_musicVolumeSlider = new GridBagConstraints();
		gbc_audio_musicVolumeSlider.fill = GridBagConstraints.HORIZONTAL;
		gbc_audio_musicVolumeSlider.gridx = 2;
		gbc_audio_musicVolumeSlider.gridy = 3;
		audioPanel.add(audio_musicVolumeSlider, gbc_audio_musicVolumeSlider);
		
		audio_musicVolumeValue = new JLabel("50");
		GridBagConstraints gbc_audio_musicVolumeValue = new GridBagConstraints();
		gbc_audio_musicVolumeValue.insets = new Insets(0, 0, 0, 2);
		gbc_audio_musicVolumeValue.gridx = 3;
		gbc_audio_musicVolumeValue.gridy = 3;
		audioPanel.add(audio_musicVolumeValue, gbc_audio_musicVolumeValue);
		
		audio_musicVolumeReset = new JButton("Reset");
		audio_musicVolumeReset.setFocusPainted(false);
		audio_musicVolumeReset.setBorder(new EmptyBorder(4, 8, 4, 8));
		audio_musicVolumeReset.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				audio_musicVolumeSlider.setValue(50);
			}
		});
		GridBagConstraints gbc_audio_musicVolumeReset = new GridBagConstraints();
		gbc_audio_musicVolumeReset.gridx = 4;
		gbc_audio_musicVolumeReset.gridy = 3;
		audioPanel.add(audio_musicVolumeReset, gbc_audio_musicVolumeReset);
		
		audio_soundFXVolumeLabel = new JLabel("Sound FX Volume:");
		GridBagConstraints gbc_audio_sfxVolumeLabel = new GridBagConstraints();
		gbc_audio_sfxVolumeLabel.insets = new Insets(0, 0, 0, 2);
		gbc_audio_sfxVolumeLabel.anchor = GridBagConstraints.EAST;
		gbc_audio_sfxVolumeLabel.gridx = 1;
		gbc_audio_sfxVolumeLabel.gridy = 4;
		audioPanel.add(audio_soundFXVolumeLabel, gbc_audio_sfxVolumeLabel);
		
		audio_sfxVolumeSlider = new JSlider();
		audio_sfxVolumeSlider.setFocusable(false);
		audio_sfxVolumeSlider.setValue(100);
		audio_sfxVolumeSlider.setMinorTickSpacing(5);
		audio_sfxVolumeSlider.setMajorTickSpacing(25);
		audio_sfxVolumeSlider.setPaintTicks(true);
		audio_sfxVolumeSlider.setSnapToTicks(true);
		audio_sfxVolumeSlider.addChangeListener(new ChangeListener(){
			@Override
			public void stateChanged(ChangeEvent evt)
			{
				JSlider source = (JSlider)evt.getSource();
				audio_sfxVolumeValue.setText(source.getValue()+"");
			}
		});
		GridBagConstraints gbc_audio_sfxVolumeSlider = new GridBagConstraints();
		gbc_audio_sfxVolumeSlider.fill = GridBagConstraints.HORIZONTAL;
		gbc_audio_sfxVolumeSlider.gridx = 2;
		gbc_audio_sfxVolumeSlider.gridy = 4;
		audioPanel.add(audio_sfxVolumeSlider, gbc_audio_sfxVolumeSlider);
		
		audio_sfxVolumeValue = new JLabel("100");
		GridBagConstraints gbc_audio_sfxVolumeValue = new GridBagConstraints();
		gbc_audio_sfxVolumeValue.insets = new Insets(0, 0, 0, 2);
		gbc_audio_sfxVolumeValue.gridx = 3;
		gbc_audio_sfxVolumeValue.gridy = 4;
		audioPanel.add(audio_sfxVolumeValue, gbc_audio_sfxVolumeValue);
		
		audio_sfxVolumeReset = new JButton("Reset");
		audio_sfxVolumeReset.setFocusPainted(false);
		audio_sfxVolumeReset.setBorder(new EmptyBorder(4, 8, 4, 8));
		audio_sfxVolumeReset.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				audio_sfxVolumeSlider.setValue(100);
			}
		});
		GridBagConstraints gbc_audio_sfxVolumeReset = new GridBagConstraints();
		gbc_audio_sfxVolumeReset.gridx = 4;
		gbc_audio_sfxVolumeReset.gridy = 4;
		audioPanel.add(audio_sfxVolumeReset, gbc_audio_sfxVolumeReset);
		
		audio_voiceVolumeLabel = new JLabel("Voice Volume:");
		GridBagConstraints gbc_audio_voiceVolumeLabel = new GridBagConstraints();
		gbc_audio_voiceVolumeLabel.insets = new Insets(0, 0, 0, 2);
		gbc_audio_voiceVolumeLabel.anchor = GridBagConstraints.EAST;
		gbc_audio_voiceVolumeLabel.gridx = 1;
		gbc_audio_voiceVolumeLabel.gridy = 5;
		audioPanel.add(audio_voiceVolumeLabel, gbc_audio_voiceVolumeLabel);
		
		audio_voiceVolumeSlider = new JSlider();
		audio_voiceVolumeSlider.setFocusable(false);
		audio_voiceVolumeSlider.setMinorTickSpacing(5);
		audio_voiceVolumeSlider.setMajorTickSpacing(25);
		audio_voiceVolumeSlider.setPaintTicks(true);
		audio_voiceVolumeSlider.setSnapToTicks(true);
		audio_voiceVolumeSlider.addChangeListener(new ChangeListener(){
			@Override
			public void stateChanged(ChangeEvent evt)
			{
				JSlider source = (JSlider)evt.getSource();
				audio_voiceVolumeValue.setText(source.getValue()+"");
			}
		});
		GridBagConstraints gbc_audio_voiceVolumeSlider = new GridBagConstraints();
		gbc_audio_voiceVolumeSlider.fill = GridBagConstraints.HORIZONTAL;
		gbc_audio_voiceVolumeSlider.gridx = 2;
		gbc_audio_voiceVolumeSlider.gridy = 5;
		audioPanel.add(audio_voiceVolumeSlider, gbc_audio_voiceVolumeSlider);
		
		audio_voiceVolumeValue = new JLabel("50");
		GridBagConstraints gbc_audio_voiceVolumeValue = new GridBagConstraints();
		gbc_audio_voiceVolumeValue.insets = new Insets(0, 0, 0, 2);
		gbc_audio_voiceVolumeValue.gridx = 3;
		gbc_audio_voiceVolumeValue.gridy = 5;
		audioPanel.add(audio_voiceVolumeValue, gbc_audio_voiceVolumeValue);
		
		audio_voiceVolumeReset = new JButton("Reset");
		audio_voiceVolumeReset.setFocusPainted(false);
		audio_voiceVolumeReset.setBorder(new EmptyBorder(4, 8, 4, 8));
		audio_voiceVolumeReset.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				audio_voiceVolumeSlider.setValue(50);
			}
		});
		GridBagConstraints gbc_audio_voiceVolumeReset = new GridBagConstraints();
		gbc_audio_voiceVolumeReset.gridx = 4;
		gbc_audio_voiceVolumeReset.gridy = 5;
		audioPanel.add(audio_voiceVolumeReset, gbc_audio_voiceVolumeReset);
		
		audio_announcerVolumeLabel = new JLabel("Announcer Volume:");
		GridBagConstraints gbc_audio_announcerVolumeLabel = new GridBagConstraints();
		gbc_audio_announcerVolumeLabel.insets = new Insets(0, 0, 0, 2);
		gbc_audio_announcerVolumeLabel.anchor = GridBagConstraints.EAST;
		gbc_audio_announcerVolumeLabel.gridx = 1;
		gbc_audio_announcerVolumeLabel.gridy = 6;
		audioPanel.add(audio_announcerVolumeLabel, gbc_audio_announcerVolumeLabel);
		
		audio_announcerVolumeSlider = new JSlider();
		audio_announcerVolumeSlider.setFocusable(false);
		audio_announcerVolumeSlider.setMajorTickSpacing(25);
		audio_announcerVolumeSlider.setMinorTickSpacing(5);
		audio_announcerVolumeSlider.setPaintTicks(true);
		audio_announcerVolumeSlider.setSnapToTicks(true);
		audio_announcerVolumeSlider.addChangeListener(new ChangeListener(){
			@Override
			public void stateChanged(ChangeEvent evt)
			{
				JSlider source = (JSlider)evt.getSource();
				audio_announcerVolumeValue.setText(source.getValue()+"");
			}
		});
		GridBagConstraints gbc_audio_announcerVolumeSlider = new GridBagConstraints();
		gbc_audio_announcerVolumeSlider.fill = GridBagConstraints.HORIZONTAL;
		gbc_audio_announcerVolumeSlider.gridx = 2;
		gbc_audio_announcerVolumeSlider.gridy = 6;
		audioPanel.add(audio_announcerVolumeSlider, gbc_audio_announcerVolumeSlider);
		
		audio_announcerVolumeValue = new JLabel("50");
		GridBagConstraints gbc_audio_announcerVolumeValue = new GridBagConstraints();
		gbc_audio_announcerVolumeValue.insets = new Insets(0, 0, 0, 2);
		gbc_audio_announcerVolumeValue.gridx = 3;
		gbc_audio_announcerVolumeValue.gridy = 6;
		audioPanel.add(audio_announcerVolumeValue, gbc_audio_announcerVolumeValue);
		
		audio_announcerVolumeReset = new JButton("Reset");
		audio_announcerVolumeReset.setFocusPainted(false);
		audio_announcerVolumeReset.setBorder(new EmptyBorder(4, 8, 4, 8));
		audio_announcerVolumeReset.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				audio_announcerVolumeSlider.setValue(50);
			}
		});
		GridBagConstraints gbc_audio_announcerVolumeReset = new GridBagConstraints();
		gbc_audio_announcerVolumeReset.gridx = 4;
		gbc_audio_announcerVolumeReset.gridy = 6;
		audioPanel.add(audio_announcerVolumeReset, gbc_audio_announcerVolumeReset);
		
		JPanel interfacePanel = new JPanel();
		interfacePanel.setBorder(new EmptyBorder(2, 2, 2, 2));
		mainTabbedPane.addTab("Interface Settings", new ImageIcon(LoLSettingsUI.class.getResource("/resources/images/interface_settings.png")), interfacePanel, null);
		GridBagLayout gbl_interfacePanel = new GridBagLayout();
		gbl_interfacePanel.columnWidths = new int[]{0, 0, 0};
		gbl_interfacePanel.rowHeights = new int[]{0, 0, 0, 0};
		gbl_interfacePanel.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gbl_interfacePanel.rowWeights = new double[]{0.0, 1.0, 1.0, Double.MIN_VALUE};
		interfacePanel.setLayout(gbl_interfacePanel);
		
		JLabel int_titleLabel = new JLabel("Interface Settings");
		int_titleLabel.setFont(int_titleLabel.getFont().deriveFont(int_titleLabel.getFont().getStyle() | Font.BOLD, int_titleLabel.getFont().getSize() + 4f));
		GridBagConstraints gbc_int_titleLabel = new GridBagConstraints();
		gbc_int_titleLabel.insets = new Insets(0, 0, 2, 0);
		gbc_int_titleLabel.anchor = GridBagConstraints.WEST;
		gbc_int_titleLabel.gridx = 0;
		gbc_int_titleLabel.gridy = 0;
		interfacePanel.add(int_titleLabel, gbc_int_titleLabel);
		
		JPanel int_hudPanel = new JPanel();
		int_hudPanel.setBorder(new TitledBorder(null, "HUD Settings", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_int_hudPanel = new GridBagConstraints();
		gbc_int_hudPanel.gridheight = 2;
		gbc_int_hudPanel.fill = GridBagConstraints.BOTH;
		gbc_int_hudPanel.gridx = 0;
		gbc_int_hudPanel.gridy = 1;
		interfacePanel.add(int_hudPanel, gbc_int_hudPanel);
		GridBagLayout gbl_int_hudPanel = new GridBagLayout();
		gbl_int_hudPanel.columnWidths = new int[]{61, 0, 0, 0, 0, 0};
		gbl_int_hudPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_int_hudPanel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, 0.0};
		gbl_int_hudPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		int_hudPanel.setLayout(gbl_int_hudPanel);
		
		int_oldHudCheckBox = new JCheckBox("Use old HUD (pre-Season 3)");
		int_oldHudCheckBox.setFocusPainted(false);
		int_oldHudCheckBox.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent evt)
			{
				boolean deselected = evt.getStateChange() == ItemEvent.DESELECTED;
				int_hudAnimations.setEnabled(deselected);
				int_shopDisplayModeList.setEnabled(deselected);      
				int_shopDisplayModeGrid.setEnabled(deselected);      
				int_shopStartPanelRecommended.setEnabled(deselected);
				int_shopStartPanelAll.setEnabled(deselected);        
			}
		});
		GridBagConstraints gbc_int_oldHudCheckBox = new GridBagConstraints();
		gbc_int_oldHudCheckBox.anchor = GridBagConstraints.WEST;
		gbc_int_oldHudCheckBox.gridwidth = 6;
		gbc_int_oldHudCheckBox.gridx = 0;
		gbc_int_oldHudCheckBox.gridy = 0;
		int_hudPanel.add(int_oldHudCheckBox, gbc_int_oldHudCheckBox);
		
		JTextPane int_oldHudTip = new JTextPane();
		int_oldHudTip.setText("Use of the old HUD is not supported and may have unexpected consequences. Use at your own risk.");
		int_oldHudTip.setEnabled(false);
		int_oldHudTip.setEditable(false);
		GridBagConstraints gbc_int_oldHudTip = new GridBagConstraints();
		gbc_int_oldHudTip.insets = new Insets(0, 0, 4, 0);
		gbc_int_oldHudTip.gridwidth = 6;
		gbc_int_oldHudTip.fill = GridBagConstraints.BOTH;
		gbc_int_oldHudTip.gridx = 0;
		gbc_int_oldHudTip.gridy = 1;
		int_hudPanel.add(int_oldHudTip, gbc_int_oldHudTip);
		
		JLabel int_hudScaleLabel = new JLabel("HUD Scale:");
		GridBagConstraints gbc_int_hudScaleLabel = new GridBagConstraints();
		gbc_int_hudScaleLabel.insets = new Insets(0, 0, 0, 2);
		gbc_int_hudScaleLabel.anchor = GridBagConstraints.EAST;
		gbc_int_hudScaleLabel.gridx = 0;
		gbc_int_hudScaleLabel.gridy = 2;
		int_hudPanel.add(int_hudScaleLabel, gbc_int_hudScaleLabel);
		
		int_hudScaleSlider = new JSlider();
		int_hudScaleSlider.setFocusable(false);
		int_hudScaleSlider.setValue(100);
		int_hudScaleSlider.setMajorTickSpacing(25);
		int_hudScaleSlider.setPaintTicks(true);
		int_hudScaleSlider.setMinorTickSpacing(5);
		GridBagConstraints gbc_int_hudScaleSlider = new GridBagConstraints();
		gbc_int_hudScaleSlider.gridwidth = 4;
		gbc_int_hudScaleSlider.fill = GridBagConstraints.HORIZONTAL;
		gbc_int_hudScaleSlider.gridx = 1;
		gbc_int_hudScaleSlider.gridy = 2;
		int_hudPanel.add(int_hudScaleSlider, gbc_int_hudScaleSlider);
		
		int_hudScaleReset = new JButton("Reset");
		int_hudScaleReset.setFocusPainted(false);
		int_hudScaleReset.setBorder(new EmptyBorder(4, 8, 4, 8));
		int_hudScaleReset.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				int_hudScaleSlider.setValue(int_hudScaleSlider.getMaximum());
			}
		});
		GridBagConstraints gbc_int_hudScaleReset = new GridBagConstraints();
		gbc_int_hudScaleReset.gridx = 5;
		gbc_int_hudScaleReset.gridy = 2;
		int_hudPanel.add(int_hudScaleReset, gbc_int_hudScaleReset);
		
		int_hideWallCheckBox = new JCheckBox("Hide HUD wall");
		int_hideWallCheckBox.setFocusPainted(false);
		GridBagConstraints gbc_int_hideWallCheckBox = new GridBagConstraints();
		gbc_int_hideWallCheckBox.insets = new Insets(0, 0, 4, 0);
		gbc_int_hideWallCheckBox.anchor = GridBagConstraints.WEST;
		gbc_int_hideWallCheckBox.gridwidth = 3;
		gbc_int_hideWallCheckBox.gridx = 0;
		gbc_int_hideWallCheckBox.gridy = 3;
		int_hudPanel.add(int_hideWallCheckBox, gbc_int_hideWallCheckBox);
		
		int_hudAnimations = new JCheckBox("Enable HUD animations");
		int_hudAnimations.setFocusPainted(false);
		GridBagConstraints gbc_int_hudAnimations = new GridBagConstraints();
		gbc_int_hudAnimations.anchor = GridBagConstraints.WEST;
		gbc_int_hudAnimations.insets = new Insets(0, 0, 4, 0);
		gbc_int_hudAnimations.gridwidth = 3;
		gbc_int_hudAnimations.gridx = 3;
		gbc_int_hudAnimations.gridy = 3;
		int_hudPanel.add(int_hudAnimations, gbc_int_hudAnimations);
		
		int_showTipsCheckBox = new JCheckBox("Show newbie tips");
		int_showTipsCheckBox.setFocusPainted(false);
		GridBagConstraints gbc_int_showTipsCheckBox = new GridBagConstraints();
		gbc_int_showTipsCheckBox.gridwidth = 3;
		gbc_int_showTipsCheckBox.anchor = GridBagConstraints.WEST;
		gbc_int_showTipsCheckBox.gridx = 0;
		gbc_int_showTipsCheckBox.gridy = 4;
		int_hudPanel.add(int_showTipsCheckBox, gbc_int_showTipsCheckBox);
		
		int_showHPCheckBox = new JCheckBox("Show HP bars");
		int_showHPCheckBox.setFocusPainted(false);
		GridBagConstraints gbc_int_showHPCheckBox = new GridBagConstraints();
		gbc_int_showHPCheckBox.insets = new Insets(0, 0, 2, 0);
		gbc_int_showHPCheckBox.gridwidth = 3;
		gbc_int_showHPCheckBox.anchor = GridBagConstraints.WEST;
		gbc_int_showHPCheckBox.gridx = 3;
		gbc_int_showHPCheckBox.gridy = 4;
		int_hudPanel.add(int_showHPCheckBox, gbc_int_showHPCheckBox);
		
		JLabel int_cooldownLabel = new JLabel("Cooldown display:");
		GridBagConstraints gbc_int_cooldownLabel = new GridBagConstraints();
		gbc_int_cooldownLabel.insets = new Insets(0, 0, 6, 2);
		gbc_int_cooldownLabel.gridwidth = 2;
		gbc_int_cooldownLabel.anchor = GridBagConstraints.EAST;
		gbc_int_cooldownLabel.gridx = 0;
		gbc_int_cooldownLabel.gridy = 5;
		int_hudPanel.add(int_cooldownLabel, gbc_int_cooldownLabel);
		
		int_cooldownComboBox = new JComboBox<String>();
		int_cooldownComboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"None", "Sec", "Min:Sec", "Simplified"}));
		int_cooldownComboBox.setRenderer(new BasicStripedListCellRenderer());
		GridBagConstraints gbc_int_cooldownComboBox = new GridBagConstraints();
		gbc_int_cooldownComboBox.insets = new Insets(0, 0, 6, 0);
		gbc_int_cooldownComboBox.gridwidth = 3;
		gbc_int_cooldownComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_int_cooldownComboBox.gridx = 2;
		gbc_int_cooldownComboBox.gridy = 5;
		int_hudPanel.add(int_cooldownComboBox, gbc_int_cooldownComboBox);
		
		JLabel int_shopDisplayModeLabel = new JLabel("Shop display mode:");
		GridBagConstraints gbc_int_shopDisplayModeLabel = new GridBagConstraints();
		gbc_int_shopDisplayModeLabel.anchor = GridBagConstraints.EAST;
		gbc_int_shopDisplayModeLabel.insets = new Insets(0, 2, 0, 2);
		gbc_int_shopDisplayModeLabel.gridwidth = 2;
		gbc_int_shopDisplayModeLabel.gridx = 0;
		gbc_int_shopDisplayModeLabel.gridy = 6;
		int_hudPanel.add(int_shopDisplayModeLabel, gbc_int_shopDisplayModeLabel);
		
		int_shopDisplayModeList = new JRadioButton("List");
		int_shopDisplayModeList.setFocusPainted(false);
		int_shopDisplayModeList.setSelected(true);
		int_shopDisplayModeButtonGroup.add(int_shopDisplayModeList);
		GridBagConstraints gbc_int_shopDisplayModeList = new GridBagConstraints();
		gbc_int_shopDisplayModeList.anchor = GridBagConstraints.WEST;
		gbc_int_shopDisplayModeList.gridx = 2;
		gbc_int_shopDisplayModeList.gridy = 6;
		int_hudPanel.add(int_shopDisplayModeList, gbc_int_shopDisplayModeList);
		
		int_shopDisplayModeGrid = new JRadioButton("Grid");
		int_shopDisplayModeGrid.setFocusPainted(false);
		int_shopDisplayModeButtonGroup.add(int_shopDisplayModeGrid);
		GridBagConstraints gbc_int_shopDisplayModeGrid = new GridBagConstraints();
		gbc_int_shopDisplayModeGrid.gridwidth = 2;
		gbc_int_shopDisplayModeGrid.anchor = GridBagConstraints.WEST;
		gbc_int_shopDisplayModeGrid.gridx = 4;
		gbc_int_shopDisplayModeGrid.gridy = 6;
		int_hudPanel.add(int_shopDisplayModeGrid, gbc_int_shopDisplayModeGrid);
		
		JLabel int_shopStartPanelLabel = new JLabel("Shop start pane:");
		GridBagConstraints gbc_int_shopStartPanelLabel = new GridBagConstraints();
		gbc_int_shopStartPanelLabel.insets = new Insets(0, 0, 0, 2);
		gbc_int_shopStartPanelLabel.anchor = GridBagConstraints.EAST;
		gbc_int_shopStartPanelLabel.gridwidth = 2;
		gbc_int_shopStartPanelLabel.gridx = 0;
		gbc_int_shopStartPanelLabel.gridy = 7;
		int_hudPanel.add(int_shopStartPanelLabel, gbc_int_shopStartPanelLabel);
		
		int_shopStartPanelRecommended = new JRadioButton("Recommended");
		int_shopStartPanelRecommended.setFocusPainted(false);
		int_shopStartPanelRecommended.setSelected(true);
		int_shopStartPaneButtonGroup.add(int_shopStartPanelRecommended);
		GridBagConstraints gbc_int_shopStartPanelRecommended = new GridBagConstraints();
		gbc_int_shopStartPanelRecommended.anchor = GridBagConstraints.WEST;
		gbc_int_shopStartPanelRecommended.gridwidth = 2;
		gbc_int_shopStartPanelRecommended.gridx = 2;
		gbc_int_shopStartPanelRecommended.gridy = 7;
		int_hudPanel.add(int_shopStartPanelRecommended, gbc_int_shopStartPanelRecommended);
		
		int_shopStartPanelAll = new JRadioButton("All items");
		int_shopStartPanelAll.setFocusPainted(false);
		int_shopStartPaneButtonGroup.add(int_shopStartPanelAll);
		GridBagConstraints gbc_int_shopStartPanelAll = new GridBagConstraints();
		gbc_int_shopStartPanelAll.gridwidth = 2;
		gbc_int_shopStartPanelAll.anchor = GridBagConstraints.WEST;
		gbc_int_shopStartPanelAll.gridx = 4;
		gbc_int_shopStartPanelAll.gridy = 7;
		int_hudPanel.add(int_shopStartPanelAll, gbc_int_shopStartPanelAll);
		
		JPanel int_minimapPanel = new JPanel();
		int_minimapPanel.setBorder(new TitledBorder(null, "Minimap Settings", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_int_minimapPanel = new GridBagConstraints();
		gbc_int_minimapPanel.gridheight = 2;
		gbc_int_minimapPanel.fill = GridBagConstraints.BOTH;
		gbc_int_minimapPanel.gridx = 1;
		gbc_int_minimapPanel.gridy = 0;
		interfacePanel.add(int_minimapPanel, gbc_int_minimapPanel);
		GridBagLayout gbl_int_minimapPanel = new GridBagLayout();
		gbl_int_minimapPanel.columnWidths = new int[]{0, 0, 0, 0};
		gbl_int_minimapPanel.rowHeights = new int[]{0, 0, 0, 0};
		gbl_int_minimapPanel.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_int_minimapPanel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		int_minimapPanel.setLayout(gbl_int_minimapPanel);
		
		JLabel int_mapScaleLabel = new JLabel("Map Scale:");
		GridBagConstraints gbc_int_mapScaleLabel = new GridBagConstraints();
		gbc_int_mapScaleLabel.anchor = GridBagConstraints.EAST;
		gbc_int_mapScaleLabel.insets = new Insets(0, 2, 0, 2);
		gbc_int_mapScaleLabel.gridx = 0;
		gbc_int_mapScaleLabel.gridy = 0;
		int_minimapPanel.add(int_mapScaleLabel, gbc_int_mapScaleLabel);
		
		int_mapScaleSlider = new JSlider();
		int_mapScaleSlider.setFocusable(false);
		int_mapScaleSlider.setValue(100);
		int_mapScaleSlider.setMajorTickSpacing(25);
		int_mapScaleSlider.setMinorTickSpacing(5);
		int_mapScaleSlider.setPaintTicks(true);
		GridBagConstraints gbc_int_mapScaleSlider = new GridBagConstraints();
		gbc_int_mapScaleSlider.fill = GridBagConstraints.HORIZONTAL;
		gbc_int_mapScaleSlider.gridx = 1;
		gbc_int_mapScaleSlider.gridy = 0;
		int_minimapPanel.add(int_mapScaleSlider, gbc_int_mapScaleSlider);
		
		int_mapScaleReset = new JButton("Reset");
		int_mapScaleReset.setFocusPainted(false);
		int_mapScaleReset.setBorder(new EmptyBorder(4, 8, 4, 8));
		int_mapScaleReset.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				int_mapScaleSlider.setValue(int_mapScaleSlider.getMaximum());
			}
		});
		GridBagConstraints gbc_int_mapScaleReset = new GridBagConstraints();
		gbc_int_mapScaleReset.gridx = 2;
		gbc_int_mapScaleReset.gridy = 0;
		int_minimapPanel.add(int_mapScaleReset, gbc_int_mapScaleReset);
		
		int_flipMinimapCheckBox = new JCheckBox("Flip Minimap");
		int_flipMinimapCheckBox.setFocusPainted(false);
		GridBagConstraints gbc_int_flipMinimapCheckBox = new GridBagConstraints();
		gbc_int_flipMinimapCheckBox.anchor = GridBagConstraints.WEST;
		gbc_int_flipMinimapCheckBox.gridwidth = 2;
		gbc_int_flipMinimapCheckBox.gridx = 0;
		gbc_int_flipMinimapCheckBox.gridy = 1;
		int_minimapPanel.add(int_flipMinimapCheckBox, gbc_int_flipMinimapCheckBox);
		
		int_showNeutralsCheckBox = new JCheckBox("Show neutral camps");
		int_showNeutralsCheckBox.setEnabled(false);
		int_showNeutralsCheckBox.setSelected(true);
		int_showNeutralsCheckBox.setFocusPainted(false);
		GridBagConstraints gbc_int_showNeutralsCheckBox = new GridBagConstraints();
		gbc_int_showNeutralsCheckBox.anchor = GridBagConstraints.WEST;
		gbc_int_showNeutralsCheckBox.gridwidth = 2;
		gbc_int_showNeutralsCheckBox.gridx = 0;
		gbc_int_showNeutralsCheckBox.gridy = 2;
		int_minimapPanel.add(int_showNeutralsCheckBox, gbc_int_showNeutralsCheckBox);
		
		JPanel int_chatPanel = new JPanel();
		int_chatPanel.setBorder(new TitledBorder(null, "Chat Settings", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_int_chatPanel = new GridBagConstraints();
		gbc_int_chatPanel.fill = GridBagConstraints.BOTH;
		gbc_int_chatPanel.gridx = 1;
		gbc_int_chatPanel.gridy = 2;
		interfacePanel.add(int_chatPanel, gbc_int_chatPanel);
		GridBagLayout gbl_int_chatPanel = new GridBagLayout();
		gbl_int_chatPanel.columnWidths = new int[]{0, 0, 0, 0};
		gbl_int_chatPanel.rowHeights = new int[]{0, 0, 0, 0};
		gbl_int_chatPanel.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_int_chatPanel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		int_chatPanel.setLayout(gbl_int_chatPanel);
		
		JLabel int_chatScaleLabel = new JLabel("Chat Scale:");
		GridBagConstraints gbc_int_chatScaleLabel = new GridBagConstraints();
		gbc_int_chatScaleLabel.anchor = GridBagConstraints.EAST;
		gbc_int_chatScaleLabel.insets = new Insets(0, 2, 0, 2);
		gbc_int_chatScaleLabel.gridx = 0;
		gbc_int_chatScaleLabel.gridy = 0;
		int_chatPanel.add(int_chatScaleLabel, gbc_int_chatScaleLabel);
		
		int_chatScaleSlider = new JSlider();
		int_chatScaleSlider.setFocusable(false);
		int_chatScaleSlider.setMinorTickSpacing(5);
		int_chatScaleSlider.setMajorTickSpacing(25);
		int_chatScaleSlider.setValue(100);
		int_chatScaleSlider.setPaintTicks(true);
		GridBagConstraints gbc_int_chatScaleSlider = new GridBagConstraints();
		gbc_int_chatScaleSlider.fill = GridBagConstraints.HORIZONTAL;
		gbc_int_chatScaleSlider.gridx = 1;
		gbc_int_chatScaleSlider.gridy = 0;
		int_chatPanel.add(int_chatScaleSlider, gbc_int_chatScaleSlider);
		
		int_chatScaleReset = new JButton("Reset");
		int_chatScaleReset.setFocusPainted(false);
		int_chatScaleReset.setBorder(new EmptyBorder(4, 8, 4, 8));
		int_chatScaleReset.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				int_chatScaleSlider.setValue(int_chatScaleSlider.getMaximum());
			}
		});
		GridBagConstraints gbc_int_chatScaleReset = new GridBagConstraints();
		gbc_int_chatScaleReset.gridx = 2;
		gbc_int_chatScaleReset.gridy = 0;
		int_chatPanel.add(int_chatScaleReset, gbc_int_chatScaleReset);
		
		int_showAllChatCheckBox = new JCheckBox("Show [All] Chat (Matched Games)");
		int_showAllChatCheckBox.setFocusPainted(false);
		GridBagConstraints gbc_int_showAllChatCheckBox = new GridBagConstraints();
		gbc_int_showAllChatCheckBox.gridwidth = 2;
		gbc_int_showAllChatCheckBox.anchor = GridBagConstraints.WEST;
		gbc_int_showAllChatCheckBox.gridx = 0;
		gbc_int_showAllChatCheckBox.gridy = 1;
		int_chatPanel.add(int_showAllChatCheckBox, gbc_int_showAllChatCheckBox);
		
		int_showTimestampsCheckBox = new JCheckBox("Show Timestamps");
		int_showTimestampsCheckBox.setFocusPainted(false);
		GridBagConstraints gbc_int_showTimestampsCheckBox = new GridBagConstraints();
		gbc_int_showTimestampsCheckBox.gridwidth = 2;
		gbc_int_showTimestampsCheckBox.anchor = GridBagConstraints.WEST;
		gbc_int_showTimestampsCheckBox.gridx = 0;
		gbc_int_showTimestampsCheckBox.gridy = 2;
		int_chatPanel.add(int_showTimestampsCheckBox, gbc_int_showTimestampsCheckBox);
		
		JPanel gamePanel = new JPanel();
		gamePanel.setBorder(new EmptyBorder(2, 2, 2, 2));
		mainTabbedPane.addTab("Game Settings", new ImageIcon(LoLSettingsUI.class.getResource("/resources/images/game_settings.png")), gamePanel, null);
		GridBagLayout gbl_gamePanel = new GridBagLayout();
		gbl_gamePanel.columnWidths = new int[]{337, 180, 0};
		gbl_gamePanel.rowHeights = new int[]{0, 0, 0, 0};
		gbl_gamePanel.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gbl_gamePanel.rowWeights = new double[]{0.0, 1.0, 1.0, Double.MIN_VALUE};
		gamePanel.setLayout(gbl_gamePanel);
		
		JLabel game_titleLabel = new JLabel("Game Settings");
		game_titleLabel.setFont(game_titleLabel.getFont().deriveFont(game_titleLabel.getFont().getStyle() | Font.BOLD, game_titleLabel.getFont().getSize() + 4f));
		GridBagConstraints gbc_game_titleLabel = new GridBagConstraints();
		gbc_game_titleLabel.insets = new Insets(0, 0, 2, 0);
		gbc_game_titleLabel.anchor = GridBagConstraints.WEST;
		gbc_game_titleLabel.gridx = 0;
		gbc_game_titleLabel.gridy = 0;
		gamePanel.add(game_titleLabel, gbc_game_titleLabel);
		
		JPanel game_generalPanel = new JPanel();
		game_generalPanel.setBorder(new TitledBorder(null, "General Settings", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_game_generalPanel = new GridBagConstraints();
		gbc_game_generalPanel.fill = GridBagConstraints.BOTH;
		gbc_game_generalPanel.gridx = 0;
		gbc_game_generalPanel.gridy = 1;
		gamePanel.add(game_generalPanel, gbc_game_generalPanel);
		GridBagLayout gbl_game_generalPanel = new GridBagLayout();
		gbl_game_generalPanel.columnWidths = new int[]{0, 0, 0};
		gbl_game_generalPanel.rowHeights = new int[]{0, 0, 0};
		gbl_game_generalPanel.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_game_generalPanel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		game_generalPanel.setLayout(gbl_game_generalPanel);
		
		game_colorblindCheckBox = new JCheckBox("Colorblind mode");
		game_colorblindCheckBox.setToolTipText("Enables colorblind mode.");
		game_colorblindCheckBox.setFocusPainted(false);
		GridBagConstraints gbc_game_colorblindCheckBox = new GridBagConstraints();
		gbc_game_colorblindCheckBox.anchor = GridBagConstraints.WEST;
		gbc_game_colorblindCheckBox.gridx = 0;
		gbc_game_colorblindCheckBox.gridy = 0;
		game_generalPanel.add(game_colorblindCheckBox, gbc_game_colorblindCheckBox);
		
		game_disableCameraSnap = new JCheckBox("Disable camera snap on respawn");
		game_disableCameraSnap.setToolTipText("Disables the camera forcably focusing on the respawn point upon respawning.");
		GridBagConstraints gbc_game_disableCameraSnap = new GridBagConstraints();
		gbc_game_disableCameraSnap.anchor = GridBagConstraints.WEST;
		gbc_game_disableCameraSnap.gridx = 1;
		gbc_game_disableCameraSnap.gridy = 0;
		game_generalPanel.add(game_disableCameraSnap, gbc_game_disableCameraSnap);
		
		game_damageFlashCheckBox = new JCheckBox("Flash red when damaged");
		game_damageFlashCheckBox.setToolTipText("Disables/enables a red full-screen flashing effect when being damaged.");
		game_damageFlashCheckBox.setFocusPainted(false);
		GridBagConstraints gbc_game_flashRedCheckBox = new GridBagConstraints();
		gbc_game_flashRedCheckBox.anchor = GridBagConstraints.WEST;
		gbc_game_flashRedCheckBox.gridx = 0;
		gbc_game_flashRedCheckBox.gridy = 1;
		game_generalPanel.add(game_damageFlashCheckBox, gbc_game_flashRedCheckBox);
		
		JPanel game_floatingTextPanel = new JPanel();
		game_floatingTextPanel.setBorder(new TitledBorder(null, "Floating Text Settings", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_game_floatingTextPanel = new GridBagConstraints();
		gbc_game_floatingTextPanel.gridheight = 3;
		gbc_game_floatingTextPanel.fill = GridBagConstraints.BOTH;
		gbc_game_floatingTextPanel.gridx = 1;
		gbc_game_floatingTextPanel.gridy = 0;
		gamePanel.add(game_floatingTextPanel, gbc_game_floatingTextPanel);
		GridBagLayout gbl_game_floatingTextPanel = new GridBagLayout();
		gbl_game_floatingTextPanel.columnWidths = new int[]{0, 0, 0};
		gbl_game_floatingTextPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_game_floatingTextPanel.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gbl_game_floatingTextPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		game_floatingTextPanel.setLayout(gbl_game_floatingTextPanel);
		
		float_legacy = new JCheckBox("Legacy Damage Text");
		float_legacy.setFocusPainted(false);
		GridBagConstraints gbc_float_legacy = new GridBagConstraints();
		gbc_float_legacy.insets = new Insets(0, 0, 6, 0);
		gbc_float_legacy.gridwidth = 2;
		gbc_float_legacy.anchor = GridBagConstraints.WEST;
		gbc_float_legacy.gridx = 0;
		gbc_float_legacy.gridy = 0;
		game_floatingTextPanel.add(float_legacy, gbc_float_legacy);
		
		float_damage = new JCheckBox("Damage");
		float_damage.setFocusPainted(false);
		GridBagConstraints gbc_float_damage = new GridBagConstraints();
		gbc_float_damage.anchor = GridBagConstraints.WEST;
		gbc_float_damage.gridx = 0;
		gbc_float_damage.gridy = 1;
		game_floatingTextPanel.add(float_damage, gbc_float_damage);
		
		float_enemyDamage = new JCheckBox("Enemy Damage");
		float_enemyDamage.setFocusPainted(false);
		GridBagConstraints gbc_float_enemyDamage = new GridBagConstraints();
		gbc_float_enemyDamage.anchor = GridBagConstraints.WEST;
		gbc_float_enemyDamage.gridx = 1;
		gbc_float_enemyDamage.gridy = 1;
		game_floatingTextPanel.add(float_enemyDamage, gbc_float_enemyDamage);
		
		float_crit = new JCheckBox("Crits");
		GridBagConstraints gbc_float_crits = new GridBagConstraints();
		gbc_float_crits.anchor = GridBagConstraints.WEST;
		gbc_float_crits.gridx = 0;
		gbc_float_crits.gridy = 2;
		game_floatingTextPanel.add(float_crit, gbc_float_crits);
		
		float_enemyCrit = new JCheckBox("Enemy Crits");
		GridBagConstraints gbc_float_enemyCrits = new GridBagConstraints();
		gbc_float_enemyCrits.anchor = GridBagConstraints.WEST;
		gbc_float_enemyCrits.gridx = 1;
		gbc_float_enemyCrits.gridy = 2;
		game_floatingTextPanel.add(float_enemyCrit, gbc_float_enemyCrits);
		
		float_heal = new JCheckBox("Heal");
		float_heal.setFocusPainted(false);
		GridBagConstraints gbc_float_heal = new GridBagConstraints();
		gbc_float_heal.anchor = GridBagConstraints.WEST;
		gbc_float_heal.gridx = 0;
		gbc_float_heal.gridy = 3;
		game_floatingTextPanel.add(float_heal, gbc_float_heal);
		
		float_dodge = new JCheckBox("Dodge");
		float_dodge.setFocusPainted(false);
		GridBagConstraints gbc_float_dodge = new GridBagConstraints();
		gbc_float_dodge.anchor = GridBagConstraints.WEST;
		gbc_float_dodge.gridx = 1;
		gbc_float_dodge.gridy = 3;
		game_floatingTextPanel.add(float_dodge, gbc_float_dodge);
		
		float_gold = new JCheckBox("Gold");
		float_gold.setFocusPainted(false);
		GridBagConstraints gbc_float_gold = new GridBagConstraints();
		gbc_float_gold.anchor = GridBagConstraints.WEST;
		gbc_float_gold.gridx = 0;
		gbc_float_gold.gridy = 4;
		game_floatingTextPanel.add(float_gold, gbc_float_gold);
		
		float_allyGold = new JCheckBox("Ally Gold");
		GridBagConstraints gbc_allyGoldCheckBox = new GridBagConstraints();
		gbc_allyGoldCheckBox.anchor = GridBagConstraints.WEST;
		gbc_allyGoldCheckBox.gridx = 1;
		gbc_allyGoldCheckBox.gridy = 4;
		game_floatingTextPanel.add(float_allyGold, gbc_allyGoldCheckBox);
		
		float_status = new JCheckBox("Status");
		float_status.setFocusPainted(false);
		GridBagConstraints gbc_float_status = new GridBagConstraints();
		gbc_float_status.anchor = GridBagConstraints.WEST;
		gbc_float_status.gridx = 0;
		gbc_float_status.gridy = 5;
		game_floatingTextPanel.add(float_status, gbc_float_status);
		
		float_special = new JCheckBox("Special");
		float_special.setFocusPainted(false);
		GridBagConstraints gbc_float_special = new GridBagConstraints();
		gbc_float_special.anchor = GridBagConstraints.WEST;
		gbc_float_special.gridx = 1;
		gbc_float_special.gridy = 5;
		game_floatingTextPanel.add(float_special, gbc_float_special);
		
		float_mana = new JCheckBox("Mana");
		float_mana.setFocusPainted(false);
		GridBagConstraints gbc_float_mana = new GridBagConstraints();
		gbc_float_mana.anchor = GridBagConstraints.WEST;
		gbc_float_mana.gridx = 0;
		gbc_float_mana.gridy = 6;
		game_floatingTextPanel.add(float_mana, gbc_float_mana);
		
		float_level = new JCheckBox("Level");
		float_level.setFocusPainted(false);
		GridBagConstraints gbc_float_level = new GridBagConstraints();
		gbc_float_level.anchor = GridBagConstraints.WEST;
		gbc_float_level.gridx = 1;
		gbc_float_level.gridy = 6;
		game_floatingTextPanel.add(float_level, gbc_float_level);
		
		float_quest = new JCheckBox("Quest");
		float_quest.setFocusPainted(false);
		GridBagConstraints gbc_float_quest = new GridBagConstraints();
		gbc_float_quest.anchor = GridBagConstraints.WEST;
		gbc_float_quest.gridx = 0;
		gbc_float_quest.gridy = 7;
		game_floatingTextPanel.add(float_quest, gbc_float_quest);
		
		float_experience = new JCheckBox("Experience");
		float_experience.setFocusPainted(false);
		GridBagConstraints gbc_float_experience = new GridBagConstraints();
		gbc_float_experience.anchor = GridBagConstraints.WEST;
		gbc_float_experience.gridx = 1;
		gbc_float_experience.gridy = 7;
		game_floatingTextPanel.add(float_experience, gbc_float_experience);
		
		float_score = new JCheckBox("Score");
		float_score.setFocusPainted(false);
		GridBagConstraints gbc_float_score = new GridBagConstraints();
		gbc_float_score.anchor = GridBagConstraints.WEST;
		gbc_float_score.gridx = 0;
		gbc_float_score.gridy = 8;
		game_floatingTextPanel.add(float_score, gbc_float_score);
		
		JPanel game_controlPanel = new JPanel();
		game_controlPanel.setBorder(new TitledBorder(null, "Control Settings", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_game_controlPanel = new GridBagConstraints();
		gbc_game_controlPanel.fill = GridBagConstraints.BOTH;
		gbc_game_controlPanel.gridx = 0;
		gbc_game_controlPanel.gridy = 2;
		gamePanel.add(game_controlPanel, gbc_game_controlPanel);
		GridBagLayout gbl_game_controlPanel = new GridBagLayout();
		gbl_game_controlPanel.columnWidths = new int[]{0, 0, 0, 0};
		gbl_game_controlPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_game_controlPanel.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_game_controlPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		game_controlPanel.setLayout(gbl_game_controlPanel);
		
		game_movementPredictCheckBox = new JCheckBox("Use movement prediction");
		game_movementPredictCheckBox.setFocusPainted(false);
		GridBagConstraints gbc_game_movementPredictCheckBox = new GridBagConstraints();
		gbc_game_movementPredictCheckBox.gridwidth = 2;
		gbc_game_movementPredictCheckBox.anchor = GridBagConstraints.WEST;
		gbc_game_movementPredictCheckBox.gridx = 0;
		gbc_game_movementPredictCheckBox.gridy = 0;
		game_controlPanel.add(game_movementPredictCheckBox, gbc_game_movementPredictCheckBox);
		
		game_autoDisplayTargetCheckBox = new JCheckBox("Auto-display target");
		game_autoDisplayTargetCheckBox.setSelected(true);
		game_autoDisplayTargetCheckBox.setEnabled(false);
		game_autoDisplayTargetCheckBox.setFocusPainted(false);
		GridBagConstraints gbc_game_autoDisplayTargetCheckBox = new GridBagConstraints();
		gbc_game_autoDisplayTargetCheckBox.gridwidth = 2;
		gbc_game_autoDisplayTargetCheckBox.anchor = GridBagConstraints.WEST;
		gbc_game_autoDisplayTargetCheckBox.gridx = 0;
		gbc_game_autoDisplayTargetCheckBox.gridy = 1;
		game_controlPanel.add(game_autoDisplayTargetCheckBox, gbc_game_autoDisplayTargetCheckBox);
		
		game_lineMissileDisplayCheckBox = new JCheckBox("Line missile display");
		game_lineMissileDisplayCheckBox.setFocusPainted(false);
		GridBagConstraints gbc_game_lineMissileDisplayCheckBox = new GridBagConstraints();
		gbc_game_lineMissileDisplayCheckBox.gridwidth = 2;
		gbc_game_lineMissileDisplayCheckBox.anchor = GridBagConstraints.WEST;
		gbc_game_lineMissileDisplayCheckBox.gridx = 0;
		gbc_game_lineMissileDisplayCheckBox.gridy = 2;
		game_controlPanel.add(game_lineMissileDisplayCheckBox, gbc_game_lineMissileDisplayCheckBox);
		
		game_smartcastRangeIndicatorsCheckBox = new JCheckBox("Smartcast range indicators");
		game_smartcastRangeIndicatorsCheckBox.setFocusPainted(false);
		GridBagConstraints gbc_game_smartcastRangeIndicatorsCheckBox = new GridBagConstraints();
		gbc_game_smartcastRangeIndicatorsCheckBox.insets = new Insets(0, 0, 8, 0);
		gbc_game_smartcastRangeIndicatorsCheckBox.gridwidth = 2;
		gbc_game_smartcastRangeIndicatorsCheckBox.anchor = GridBagConstraints.WEST;
		gbc_game_smartcastRangeIndicatorsCheckBox.gridx = 0;
		gbc_game_smartcastRangeIndicatorsCheckBox.gridy = 3;
		game_controlPanel.add(game_smartcastRangeIndicatorsCheckBox, gbc_game_smartcastRangeIndicatorsCheckBox);
		
		JLabel game_scrollSpeedLabel = new JLabel("Map scroll speed:");
		GridBagConstraints gbc_game_scrollSpeedLabel = new GridBagConstraints();
		gbc_game_scrollSpeedLabel.anchor = GridBagConstraints.WEST;
		gbc_game_scrollSpeedLabel.gridx = 0;
		gbc_game_scrollSpeedLabel.gridy = 4;
		game_controlPanel.add(game_scrollSpeedLabel, gbc_game_scrollSpeedLabel);
		
		game_scrollSpeedSlider = new JSlider();
		game_scrollSpeedSlider.setFocusable(false);
		game_scrollSpeedSlider.addChangeListener(new ChangeListener(){
			@Override
			public void stateChanged(ChangeEvent evt)
			{
				JSlider source = (JSlider)evt.getSource();
				game_scrollSpeedValue.setText(source.getValue()+"");
			}
		});
		GridBagConstraints gbc_game_scrollSpeedSlider = new GridBagConstraints();
		gbc_game_scrollSpeedSlider.fill = GridBagConstraints.HORIZONTAL;
		gbc_game_scrollSpeedSlider.gridx = 1;
		gbc_game_scrollSpeedSlider.gridy = 4;
		game_controlPanel.add(game_scrollSpeedSlider, gbc_game_scrollSpeedSlider);
		
		game_scrollSpeedValue = new JLabel("50");
		GridBagConstraints gbc_gane_scrollSpeedValue = new GridBagConstraints();
		gbc_gane_scrollSpeedValue.insets = new Insets(0, 4, 0, 4);
		gbc_gane_scrollSpeedValue.gridx = 2;
		gbc_gane_scrollSpeedValue.gridy = 4;
		game_controlPanel.add(game_scrollSpeedValue, gbc_gane_scrollSpeedValue);
		
		game_cameraSmoothingCheckBox = new JCheckBox("Camera smoothing");
		game_cameraSmoothingCheckBox.setToolTipText("Enables/disables the smoothing of camera movement.");
		game_cameraSmoothingCheckBox.setFocusPainted(false);
		GridBagConstraints gbc_game_cameraSmoothingCheckBox = new GridBagConstraints();
		gbc_game_cameraSmoothingCheckBox.anchor = GridBagConstraints.WEST;
		gbc_game_cameraSmoothingCheckBox.gridwidth = 2;
		gbc_game_cameraSmoothingCheckBox.gridx = 0;
		gbc_game_cameraSmoothingCheckBox.gridy = 5;
		game_controlPanel.add(game_cameraSmoothingCheckBox, gbc_game_cameraSmoothingCheckBox);
		
		setLocationRelativeTo(null);
	}
	
	private void initMenuBar(Preferences prefs)
	{
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu fileMenu = new JMenu(" File ");
		fileMenu.setMnemonic('f');
		menuBar.add(fileMenu);
		
		JMenuItem exportMenuItem = new JMenuItem("Export settings...");
		exportMenuItem.setMnemonic('e');
		exportMenuItem.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				main.gui_export();
			}
		});
		//TODO
		//fileMenu.add(exportMenuItem);
		
		JMenuItem importMenuItem = new JMenuItem("Import settings...");
		importMenuItem.setMnemonic('i');
		importMenuItem.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				main.gui_import();
			}
		});
		//fileMenu.add(importMenuItem);
		
		//fileMenu.addSeparator();
		
		JMenuItem saveMenuItem = new JMenuItem("Save");
		saveMenuItem.setMnemonic('s');
		saveMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
		saveMenuItem.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				main.gui_save();
			}
		});
		fileMenu.add(saveMenuItem);
		
		JMenuItem reloadMenuItem = new JMenuItem("Reload");
		reloadMenuItem.setMnemonic('r');
		reloadMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK));
		reloadMenuItem.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				main.gui_reload();
			}
		});
		fileMenu.add(reloadMenuItem);
		
		fileMenu.addSeparator();
		
		JMenuItem resetDefaultMenuItem = new JMenuItem("Reset to defaults");
		resetDefaultMenuItem.setMnemonic('d');
		resetDefaultMenuItem.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				main.gui_resetDefault();
			}
		});
		fileMenu.add(resetDefaultMenuItem);
		
		JMenuItem resetRecommendedMenuItem = new JMenuItem("Reset to recommended");
		resetRecommendedMenuItem.setMnemonic('c');
		resetRecommendedMenuItem.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				main.gui_resetRecommended();
			}
		});
		fileMenu.add(resetRecommendedMenuItem);
		
		fileMenu.addSeparator();
		
		JMenuItem quitMenuItem = new JMenuItem("Quit");
		quitMenuItem.setMnemonic('q');
		quitMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_MASK));
		quitMenuItem.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				main.gui_close();
			}
		});
		fileMenu.add(quitMenuItem);
		
		JMenu optionsMenu = new JMenu(" Options ");
		optionsMenu.setMnemonic('o');
		menuBar.add(optionsMenu);
		
		JMenu updateMenu = new JMenu("Updating");
		updateMenu.setMnemonic('u');
		optionsMenu.add(updateMenu);
		
		JCheckBoxMenuItem updateStartupMenuItem = new JCheckBoxMenuItem("Check for updates on startup");
		updateStartupMenuItem.setMnemonic('s');
		updateStartupMenuItem.setSelected(prefs.checkVersion);
		updateStartupMenuItem.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent evt)
			{
				main.gui_setStartupVersionCheck(evt.getStateChange() == ItemEvent.SELECTED);
			}
		});
		updateMenu.add(updateStartupMenuItem);
		
		updateMenu.addSeparator();
		
		JMenuItem updateProgramMenuItem = new JMenuItem("Check for updates");
		updateProgramMenuItem.setMnemonic('u');
		updateProgramMenuItem.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				main.gui_startUpdater();
			}
		});
		updateMenu.add(updateProgramMenuItem);
		
		JMenuItem manualPathMenuItem = new JMenuItem("Manually set LoL path");
		manualPathMenuItem.setMnemonic('p');
		manualPathMenuItem.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				main.gui_openPathDialog();
			}
		});
		optionsMenu.add(manualPathMenuItem);
		
		JMenu helpMenu = new JMenu(" Help ");
		helpMenu.setMnemonic('h');
		menuBar.add(helpMenu);
		
		JMenuItem helpMenuItem = new JMenuItem("Help...");
		helpMenuItem.setMnemonic('h');
		helpMenuItem.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				main.gui_help();
			}
		});
		helpMenu.add(helpMenuItem);
		
		helpMenu.addSeparator();
		
		JMenuItem aboutMenuItem = new JMenuItem("About...");
		aboutMenuItem.setMnemonic('a');
		aboutMenuItem.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				main.gui_about();
			}
		});
		helpMenu.add(aboutMenuItem);
		
		JMenuItem changeLogMenuItem = new JMenuItem("Change log...");
		changeLogMenuItem.setMnemonic('c');
		changeLogMenuItem.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				main.gui_changelog();
			}
		});
		helpMenu.add(changeLogMenuItem);
		
		JMenuItem donateMenuItem = new JMenuItem("Donate...");
		donateMenuItem.setMnemonic('d');
		donateMenuItem.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				main.gui_donate();
			}
		});
		helpMenu.add(donateMenuItem);
		
		helpMenu.addSeparator();
		
		JMenuItem moreToolsMenuItem = new JMenuItem("More tools...");
		moreToolsMenuItem.setMnemonic('m');
		moreToolsMenuItem.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				main.gui_moreTools();
			}
		});
		helpMenu.add(moreToolsMenuItem);
		
		Component horizontalGlue = Box.createHorizontalGlue();
		menuBar.add(horizontalGlue);
		
		JLabel versionLabel = new JLabel("Version "+version+" ");
		versionLabel.setEnabled(false);
		menuBar.add(versionLabel);
	}
	
	//Update methods
	
	public void updateTitle(boolean saved)
	{
		setTitle(titleBase+(saved ? "" : " *"));
	}
	
	public void setInfoText(String text, boolean isBad)
	{
		statusLabel.setText(text);
		statusLabel.setForeground(isBad ? Color.red : colorblindEnabled ? Color.blue.darker() : Color.green.darker());
	}
	
	//Set methods
	
		//Video settings
	
	public void setResolutions(DisplayMode[] resolutions)
	{
		video_resolutionComboBox.removeAllItems();
		for(DisplayMode res : resolutions)
			video_resolutionComboBox.addItem(res);
	}
	
	public void setRecommendedResolution(DisplayMode res)
	{
		video_resolutionRenderer.setRecommendedResolution(res);
	}
	
	public void setSelectedResolution(DisplayMode res)
	{
		video_resolutionComboBox.setSelectedItem(res);
	}
	
	public void setWindowMode(int mode)
	{
		video_windowModeComboBox.setSelectedIndex(mode);
	}
	
	public void setVerticalSync(boolean vsync)
	{
		video_verticalSyncCheckBox.setSelected(vsync);
	}
	
	public void setFramerate(int type)
	{
		video_framerateComboBox.setSelectedIndex(type);
	}
	
	public void setCharacterQuality(int quality)
	{
		video_characterQualityRadioButtons[quality].setSelected(true);
		video_presetsComboBox.setSelectedIndex(video_presetsComboBox.getItemCount()-1);
	}
	
	public void setEnvironmentQuality(int quality)
	{
		video_environmentQualityRadioButtons[quality].setSelected(true);
		video_presetsComboBox.setSelectedIndex(video_presetsComboBox.getItemCount()-1);
	}
	
	public void setEffectQuality(int quality)
	{
		video_effectsQualityRadioButtons[quality].setSelected(true);
		video_presetsComboBox.setSelectedIndex(video_presetsComboBox.getItemCount()-1);
	}
	
	public void setShadowQuality(int quality)
	{
		video_shadowQualityRadioButtons[quality].setSelected(true);
		video_presetsComboBox.setSelectedIndex(video_presetsComboBox.getItemCount()-1);
	}
	
	public void setAdvancedLighting(boolean enable)
	{
		video_advancedLightingCheckBox.setSelected(enable);
		video_presetsComboBox.setSelectedIndex(video_presetsComboBox.getItemCount()-1);
	}
	
	public void setAdvancedReflections(boolean enable)
	{
		video_advancedReflectionsCheckBox.setSelected(enable);
		video_presetsComboBox.setSelectedIndex(video_presetsComboBox.getItemCount()-1);
	}
	
	public void setParticleOptimizations(boolean enable)
	{
		video_particleOptimizationsCheckBox.setSelected(enable);
	}
	
		//Audio settings
	
	public void setMasterVolume(float vol)
	{
		audio_masterVolumeSlider.setValue((int)(vol*100));
	}
	
	public void setMusicVolume(float vol)
	{
		audio_musicVolumeSlider.setValue((int)(vol*100));
	}
	
	public void setSfxVolume(float vol)
	{
		audio_sfxVolumeSlider.setValue((int)(vol*100));
	}
	
	public void setVoiceVolume(float vol)
	{
		audio_voiceVolumeSlider.setValue((int)(vol*100));
	}
	
	public void setAnnouncerVolume(float vol)
	{
		audio_announcerVolumeSlider.setValue((int)(vol*100));
	}
	
		//Interface
	
	public void setUseNewHud(boolean use)
	{
		int_oldHudCheckBox.setSelected(!use);
	}
	
	public void setHudScale(float scale)
	{
		int_hudScaleSlider.setValue((int)(scale*100));
	}
	
	public void setHideWall(boolean hide)
	{
		int_hideWallCheckBox.setSelected(hide);
	}
	
	public void setHudAnimations(boolean enable)
	{
		int_hudAnimations.setSelected(enable);
	}
	
	public void setShowNewbieTips(boolean show)
	{
		int_showTipsCheckBox.setSelected(show);
	}
	
	public void setShowHealthBars(boolean show)
	{
		int_showHPCheckBox.setSelected(show);
	}
	
	public void setCooldownDisplay(int index)
	{
		int_cooldownComboBox.setSelectedIndex(index);
	}
	
	public void setShopDisplayMode(int mode)
	{
		switch(mode < 0 ? 0 : mode > 1 ? 1 : mode)
		{
			case 0: int_shopDisplayModeList.setSelected(true);
				break;
			case 1: int_shopDisplayModeGrid.setSelected(true);
				break;
		}
	}
	
	public void setShopStartPane(int pane)
	{
		switch(pane < 0 ? 0 : pane > 1 ? 1 : pane)
		{
			case 0: int_shopStartPanelRecommended.setSelected(true);
				break;
			case 1: int_shopStartPanelAll.setSelected(true);
				break;
		}
	}
	
	public void setMinimapScale(float scale)
	{
		int_mapScaleSlider.setValue((int)(scale*100));
	}
	
	public void setMinimapFlipped(boolean flipped)
	{
		int_flipMinimapCheckBox.setSelected(flipped);
	}
	
	public void setShowNeutral(boolean show)
	{
		int_showNeutralsCheckBox.setSelected(show);
	}
	
	public void setChatScale(float scale)
	{
		int_chatScaleSlider.setValue((int)scale);
	}
	
	public void setShowAllChat(boolean show)
	{
		int_showAllChatCheckBox.setSelected(show);
	}
	
	public void setShowTimestamps(boolean show)
	{
		int_showTimestampsCheckBox.setSelected(show);
	}
	
		//Game
	
	public void setColorblind(boolean enable)
	{
		game_colorblindCheckBox.setSelected(enable);
		colorblindEnabled = enable;
	}
	
	public void setDamageFlash(boolean enable)
	{
		game_damageFlashCheckBox.setSelected(enable);
	}
	
	public void setRespawnSnap(boolean enable)
	{
		game_disableCameraSnap.setSelected(enable);
	}
	
	public void setMovePrediction(boolean enable)
	{
		game_movementPredictCheckBox.setSelected(enable);
	}
	
	public void setLineMissileDisplay(boolean enable)
	{
		game_lineMissileDisplayCheckBox.setSelected(enable);
	}
	
	public void setSmartcastRangeIndicators(boolean enable)
	{
		game_smartcastRangeIndicatorsCheckBox.setSelected(enable);
	}
	
	public void setScrollSpeed(float speed)
	{
		game_scrollSpeedSlider.setValue((int)(speed*100));
	}
	
	public void setSmoothScrolling(boolean enable)
	{
		game_cameraSmoothingCheckBox.setSelected(enable);
	}
	
		//Floating text
	
	public void setLegacyText(boolean enable)
	{
		float_legacy.setSelected(enable);
	}
	
	public void setDamageText(boolean enable)
	{
		float_damage.setSelected(enable);
	}
	
	public void setEnemyDamageText(boolean enable)
	{
		float_enemyDamage.setSelected(enable);
	}
	
	public void setCritText(boolean enable)
	{
		float_crit.setSelected(enable);
	}
	
	public void setEnemyCritText(boolean enable)
	{
		float_enemyCrit.setSelected(enable);
	}
	
	public void setHealText(boolean enable)
	{
		float_heal.setSelected(enable);
	}
	
	public void setDodgeText(boolean enable)
	{
		float_dodge.setSelected(enable);
	}
	
	public void setGoldText(boolean enable)
	{
		float_gold.setSelected(enable);
	}
	
	public void setAllyGoldText(boolean enable)
	{
		float_allyGold.setSelected(enable);
	}
	
	public void setLevelText(boolean enable)
	{
		float_level.setSelected(enable);
	}
	
	public void setStatusText(boolean enable)
	{
		float_status.setSelected(enable);
	}
	
	public void setSpecialText(boolean enable)
	{
		float_special.setSelected(enable);
	}
	
	public void setQuestText(boolean enable)
	{
		float_quest.setSelected(enable);
	}
	
	public void setScoreText(boolean enable)
	{
		float_score.setSelected(enable);
	}
	
	public void setManaText(boolean enable)
	{
		float_mana.setSelected(enable);
	}
	
	public void setExperienceText(boolean enable)
	{
		float_experience.setSelected(enable);
	}
	
	//Retrieve methods
	
		//Video
	
	public DisplayMode getResolution()
	{
		return (DisplayMode)video_resolutionComboBox.getSelectedObjects()[0];
	}
	
	public int getWindowMode()
	{
		return video_windowModeComboBox.getSelectedIndex();
	}
	
	public boolean getVerticalSync()
	{
		return video_verticalSyncCheckBox.isSelected();
	}
	
	public int getFramerate()
	{
		return video_framerateComboBox.getSelectedIndex();
	}
	
	public int getCharacterQuality()
	{
		int selected = -1;
		for(int n = 0; n < video_characterQualityRadioButtons.length; n++)
			if(video_characterQualityRadioButtons[n].isSelected())
				selected = n;
		return selected;
	}
	
	public int getEnvironmentQuality()
	{
		int selected = -1;
		for(int n = 0; n < video_environmentQualityRadioButtons.length; n++)
			if(video_environmentQualityRadioButtons[n].isSelected())
				selected = n;
		return selected;
	}
	
	public int getEffectsQuality()
	{
		int selected = -1;
		for(int n = 0; n < video_effectsQualityRadioButtons.length; n++)
			if(video_effectsQualityRadioButtons[n].isSelected())
				selected = n;
		return selected;
	}
	
	public int getShadowQuality()
	{
		int selected = -1;
		for(int n = 0; n < video_shadowQualityRadioButtons.length; n++)
			if(video_shadowQualityRadioButtons[n].isSelected())
				selected = n;
		return selected;
	}
	
	public boolean getAdvancedLighting()
	{
		return video_advancedLightingCheckBox.isSelected();
	}
	
	public boolean getAdvancedReflections()
	{
		return video_advancedReflectionsCheckBox.isSelected();
	}
	
	public boolean getParticleOptimizations()
	{
		return video_particleOptimizationsCheckBox.isSelected();
	}
	
		//Audio
	
	public float getMasterVolume()
	{
		return audio_masterVolumeSlider.getValue()/100f;
	}
	
	public float getMusicVolume()
	{
		return audio_musicVolumeSlider.getValue()/100f;
	}
	
	public float getSfxVolume()
	{
		return audio_sfxVolumeSlider.getValue()/100f;
	}
	
	public float getVoiceVolume()
	{
		return audio_voiceVolumeSlider.getValue()/100f;
	}
	
	public float getAnnouncerVolume()
	{
		return audio_announcerVolumeSlider.getValue()/100f;
	}
	
	public boolean getAudioEnabled()
	{
		return !audio_muteAllCheckBox.isSelected();
	}
	
		//Interface
	
	public boolean getUseNewHud()
	{
		return !int_oldHudCheckBox.isSelected();
	}
	
	public float getHudScale()
	{
		return int_hudScaleSlider.getValue()/100.0f;
	}
	
	public boolean getHideWall()
	{
		return int_hideWallCheckBox.isSelected();
	}
	
	public boolean getHudAnimations()
	{
		return int_hudAnimations.isSelected();
	}
	
	public boolean getShowNewbieTips()
	{
		return int_showTipsCheckBox.isSelected();
	}
	
	public boolean getShowHealthBars()
	{
		return int_showHPCheckBox.isSelected();
	}
	
	public int getCooldownDisplay()
	{
		return int_cooldownComboBox.getSelectedIndex();
	}
	
	public int getShopDisplayMode()
	{
		return int_shopDisplayModeList.isSelected() ? 0 : 1;
	}
	
	public int getShopStartPane()
	{
		return int_shopStartPanelRecommended.isSelected() ? 0 : 1;
	}
	
	public float getMinimapScale()
	{
		return int_mapScaleSlider.getValue()/100.0f;
	}
	
	public boolean getMinimapFlipped()
	{
		return int_flipMinimapCheckBox.isSelected();
	}
	
	public boolean getShowNeutral()
	{
		return int_showNeutralsCheckBox.isSelected();
	}
	
	public float getChatScale()
	{
		return int_chatScaleSlider.getValue();
	}
	
	public boolean getShowAllChat()
	{
		return int_showAllChatCheckBox.isSelected();
	}
	
	public boolean getShowTimestamps()
	{
		return int_showTimestampsCheckBox.isSelected();
	}
	
		//Game
	
	public boolean getColorblindEnabled()
	{
		return game_colorblindCheckBox.isSelected();
	}
	
	public boolean getDamageFlash()
	{
		return game_damageFlashCheckBox.isSelected();
	}
	
	public boolean getRespawnSnap()
	{
		return game_disableCameraSnap.isSelected();
	}
	
	public boolean getMovePrediction()
	{
		return game_movementPredictCheckBox.isSelected();
	}
	
	public boolean getLineMissileDisplay()
	{
		return game_lineMissileDisplayCheckBox.isSelected();
	}
	
	public boolean getSmartcastRangeIndicators()
	{
		return game_smartcastRangeIndicatorsCheckBox.isSelected();
	}
	
	public float getScrollSpeed()
	{
		return game_scrollSpeedSlider.getValue()/100f;
	}
	
	public boolean getSmoothScrolling()
	{
		return game_cameraSmoothingCheckBox.isSelected();
	}
	
		//Floating text
	
	public boolean getLegacyText()
	{
		return float_legacy.isSelected();
	}
	
	public boolean getDamageText()
	{
		return float_damage.isSelected();
	}
	
	public boolean getEnemyDamageText()
	{
		return float_enemyDamage.isSelected();
	}
	
	public boolean getCritText()
	{
		return float_crit.isSelected();
	}
	
	public boolean getEnemyCritText()
	{
		return float_enemyCrit.isSelected();
	}
	
	public boolean getHealText()
	{
		return float_heal.isSelected();
	}
	
	public boolean getDodgeText()
	{
		return float_dodge.isSelected();
	}
	
	public boolean getGoldText()
	{
		return float_gold.isSelected();
	}
	
	public boolean getAllyGoldText()
	{
		return float_allyGold.isSelected();
	}
	
	public boolean getLevelText()
	{
		return float_level.isSelected();
	}
	
	public boolean getStatusText()
	{
		return float_status.isSelected();
	}
	
	public boolean getSpecialText()
	{
		return float_special.isSelected();
	}
	
	public boolean getQuestText()
	{
		return float_quest.isSelected();
	}
	
	public boolean getScoreText()
	{
		return float_score.isSelected();
	}
	
	public boolean getManaText()
	{
		return float_mana.isSelected();
	}
	
	public boolean getExperienceText()
	{
		return float_experience.isSelected();
	}
}
