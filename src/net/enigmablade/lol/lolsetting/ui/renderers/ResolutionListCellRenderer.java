package net.enigmablade.lol.lolsetting.ui.renderers;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

import net.enigmablade.paradoxion.ui.renderers.*;

public class ResolutionListCellRenderer extends AbstractStripedListCellRenderer<DisplayMode>
{
	private DisplayMode recommendedResolution = null;
	
	public ResolutionListCellRenderer()
	{
		setBorder(new EmptyBorder(2, 2, 2, 2));
	}
	
	@Override
	public Component getListCellRendererComponent(JList<? extends DisplayMode> list, DisplayMode value, int index, boolean isSelected, boolean cellHasFocus)
	{
		super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
		
		String text = value != null ? value.getWidth()+" x "+value.getHeight() : "";
		if(index != -1 && recommendedResolution != null && value.equals(recommendedResolution))
			text += " (Recommended)";
		setText(text);
		
		return this;
	}
	
	public void setRecommendedResolution(DisplayMode res)
	{
		recommendedResolution = res;
	}
}
