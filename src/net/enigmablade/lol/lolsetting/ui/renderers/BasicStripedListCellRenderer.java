package net.enigmablade.lol.lolsetting.ui.renderers;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

import net.enigmablade.paradoxion.ui.renderers.*;

public class BasicStripedListCellRenderer extends AbstractStripedListCellRenderer<String>
{
	
	public BasicStripedListCellRenderer()
	{
		setBorder(new EmptyBorder(2, 2, 2, 2));
	}
	
	@Override
	public Component getListCellRendererComponent(JList<? extends String> list, String value, int index, boolean isSelected, boolean cellHasFocus)
	{
		super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
		
		setText(value);
		
		return this;
	}
}
