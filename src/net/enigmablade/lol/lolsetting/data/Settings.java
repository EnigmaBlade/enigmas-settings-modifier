package net.enigmablade.lol.lolsetting.data;

import java.util.*;

import net.enigmablade.paradoxion.io.ini.INIData.*;

public class Settings
{
	private Map<String, String> data;
	
	public Settings()
	{
		data = new HashMap<String, String>();
	}
	
	public void add(String keyTo, String keyFrom, INIGroup from, String defaultValue)
	{
		data.put(keyTo, from != null ? from.hasValue(keyFrom) ? from.getValue(keyFrom) : defaultValue : defaultValue);
		System.out.println("Added setting: "+keyTo+"="+data.get(keyTo));
	}
	
	public void set(String key, String value)
	{
		if(key == null)
			throw new IllegalArgumentException("Key cannot be null");
		
		data.put(key, value);
	}
	
	public void set(String key, boolean value)
	{
		data.put(key, value ? "1" : "0");
	}
	
	public void set(String key, int value)
	{
		data.put(key, value+"");
	}
	
	public void set(String key, float value)
	{
		data.put(key, value+"");
	}
	
	public String get(String key)
	{
		return data.get(key);
	}
	
	public boolean getBoolean(String key)
	{
		return "1".equals(get(key)) ? true : false;
	}
	
	public int getInt(String key)
	{
		return Integer.parseInt(get(key));
	}
	
	public float getFloat(String key)
	{
		return Float.parseFloat(get(key));
	}
	
	public double getDouble(String key)
	{
		return Double.parseDouble(get(key));
	}
}
