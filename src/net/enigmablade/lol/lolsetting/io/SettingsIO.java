package net.enigmablade.lol.lolsetting.io;

import java.io.*;

import net.enigmablade.paradoxion.io.ini.*;
import net.enigmablade.paradoxion.io.ini.INIData.*;
import static net.enigmablade.paradoxion.util.Logger.*;

import net.enigmablade.lol.lollib.io.*;
import net.enigmablade.lol.lolsetting.data.*;

public class SettingsIO
{
	public static Settings loadSettings()
	{
		File configFile = getConfigFile();
		writeToLog("Loading config data from file: "+configFile.getAbsolutePath());
		INIData data = INIParser.loadINIFile(configFile);
		Settings settings = new Settings();
		if(data != null)
		{
			INIGroup general = data.getGroup("General");
			INIGroup audio = data.getGroup("Volume");
			INIGroup hud = data.getGroup("HUD");
			INIGroup performance = data.getGroup("Performance");
			INIGroup floatingText = data.getGroup("FloatingText");
			INIGroup color = data.getGroup("ColorPalette");
			
			//Video settings
			settings.add("video.width", "Width", general, "1024");
			settings.add("video.height", "Height", general, "768");
			settings.add("video.windowMode", "Windowed", general, "0");
			settings.add("video.vsync", "WaitForVerticalSync", general, "0");
			settings.add("video.framerate", "FrameCapType", performance, "0");
			
			settings.add("video.autoPerformance", "AutoPerformanceSettings", performance, "1");
			settings.add("video.characterQuality", "CharacterQuality", performance, "2");
			settings.add("video.environmentQuality", "EnvironmentQuality", performance, "2");
			settings.add("video.effectsQuality", "EffectsQuality", performance, "2");
			settings.add("video.shadowQuality", "ShadowQuality", performance, "2");
			settings.add("video.shadowsEnabled", "ShadowsEnabled", performance, "1");
			settings.add("video.advancedLighting", "PerPixelPointLighting", performance, "1");
			settings.add("video.advancedReflections", "AdvancedReflection", performance, "1");
			settings.add("video.particleOptimizations", "EnableParticleOptimizations", performance, "0");
			
			//Audio settings
			settings.add("audio.master", "MasterVolume", audio, "1.0");
			settings.add("audio.music", "MusicVolume", audio, "0.5");
			settings.add("audio.sfx", "SfxVolume", audio, "1.0");
			settings.add("audio.voice", "VoiceVolume", audio, "0.5");
			settings.add("audio.announcer", "AnnouncerVolume", audio, "0.5");
			settings.add("audio.enabled", "EnableAudio", general, "1");
			
			//Interface settings
			settings.add("hud.hudNew", "UseHUD2012", hud, "1");
			
			settings.add("hud.drawWall", "DrawCenterHudWall", hud, "1");
			settings.add("hud.animations", "EnableHUDAnimations", performance, "1");
			
			settings.add("hud.newbieTips", "ObjectTooltips", hud, "1");
			settings.add("hud.healthBars", "DrawHealthBars", hud, "1");
			settings.add("hud.cooldownDisplay", "NumericCooldownFormat", hud, "1");
			
			settings.add("hud.shopDisplayMode", "ItemShopItemDisplayMode", hud, "0");
			settings.add("hud.shopStartPane", "ItemShopStartPane", hud, "0");
			
			settings.add("hud.flipMinimap", "FlipMiniMap", hud, "0");
			
			settings.add("hud.chatScale", "ChatScale", hud, "100");
			settings.add("hud.showAllChat", "ShowAllChannelChat", hud, "0");
			settings.add("hud.showTimestamps", "ShowTimestamps", hud, "0");
			
			//Game settings
			settings.add("game.colorblind", "ColorPalette", color, "0");
			settings.add("game.damageFlash", "FlashScreenWhenDamaged", hud, "1");
			settings.add("game.respawnsnap", "DisableCameraSnapOnRespawn", general, "0");
			
			settings.add("game.movePredict", "PredictMovement", general, "1");
			settings.add("game.lineMissileDisplay", "EnableLineMissileVis", hud, "1");
			settings.add("game.smartcastRange", "SmartCastOnKeyRelease", hud, "0");
			settings.add("game.scrollSpeed", "MapScrollSpeed", hud, "0.25");
			settings.add("game.scrollSmoothing", "ScrollSmoothingEnabled", hud, "1");
			
			settings.add("float.legacy", "Legacy_Enabled", floatingText, "0");
			settings.add("float.damage", "PhysicalDamage_Enabled", floatingText, "1");
			settings.add("float.enemyDamage", "EnemyPhysicalDamage_Enabled", floatingText, "1");
			settings.add("float.crit", "Critical_Enabled", floatingText, "1");
			settings.add("float.enemyCrit", "EnemyCritical_Enabled", floatingText, "1");
			settings.add("float.heal", "Heal_Enabled", floatingText, "1");
			settings.add("float.dodge", "Dodge_Enabled", floatingText, "1");
			settings.add("float.gold", "Gold_Enabled", floatingText, "1");
			settings.add("float.allyGold", "GoldOther_Enabled", floatingText, "0");
			settings.add("float.level", "Level_Enabled", floatingText, "1");
			settings.add("float.status", "Disable_Enabled", floatingText, "1");
			settings.add("float.special", "Special_Enabled", floatingText, "1");
			settings.add("float.quest", "QuestComplete_Enabled", floatingText, "1");
			settings.add("float.score", "Score_Enabled", floatingText, "1");
			settings.add("float.mana", "ManaHeal_Enabled", floatingText, "0");
			settings.add("float.experience", "Experience_Enabled", floatingText, "0");
			
			loadHudSettings(settings);
		}
		else
			writeToLog("Failed to load settings from \""+configFile.getAbsolutePath()+"\"", LoggingType.WARNING);
		
		return settings;
	}
	
	public static boolean saveSettings(Settings settings)
	{
		File configFile = getConfigFile();
		writeToLog("Saving config data to file: "+configFile.getAbsolutePath());
		INIData data = INIParser.loadINIFile(configFile);
		if(data != null)
		{
			INIGroup general = data.getGroup("General");
			if(general == null)
				general = data.addGroup("General");
			INIGroup audio = data.getGroup("Volume");
			if(audio == null)
				audio = data.addGroup("Volume");
			INIGroup hud = data.getGroup("HUD");
			if(hud == null)
				hud = data.addGroup("HUD");
			INIGroup performance = data.getGroup("Performance");
			if(performance == null)
				performance = data.addGroup("Performance");
			INIGroup floatingText = data.getGroup("FloatingText");
			if(floatingText == null)
				floatingText = data.addGroup("FloatingText");
			INIGroup color = data.getGroup("ColorPalette");
			if(color == null)
				color = data.addGroup("ColorPalette");
			
			//Video settings
			general.setValue("UserSetResolution", "1");
			general.setValue("Width", settings.get("video.width"));
			general.setValue("Height", settings.get("video.height"));
			general.setValue("Windowed", settings.get("video.windowMode"));
			general.setValue("BorderlessWindow", settings.getInt("video.windowMode") == 2 ? "1" : "0");
			general.setValue("WaitForVerticalSync", settings.get("video.vsync"));
			performance.setValue("FrameCapType", settings.get("video.framerate"));
			
			performance.setValue("AutoPerformanceSettings", settings.get("video.autoPerformance"));
			performance.setValue("CharacterQuality", settings.get("video.characterQuality"));
			performance.setValue("EnvironmentQuality", settings.get("video.environmentQuality"));
			performance.setValue("EffectsQuality", settings.get("video.effectsQuality"));
			performance.setValue("ShadowQuality", settings.get("video.shadowQuality"));
			performance.setValue("ShadowsEnabled", settings.getInt("video.shadowQuality") == 0 ? "0" : "1");
			performance.setValue("PerPixelPointLighting", settings.get("video.advancedLighting"));
			performance.setValue("AdvancedReflection", settings.get("video.advancedReflections"));
			performance.setValue("EnableParticleOptimizations", settings.get("video.particleOptimizations"));
			
			//Audio settings
			audio.setValue("MasterVolume", settings.get("audio.master"));
			audio.setValue("MusicVolume", settings.get("audio.music"));
			audio.setValue("SfxVolume", settings.get("audio.sfx"));
			audio.setValue("VoiceVolume", settings.get("audio.voice"));
			audio.setValue("AnnouncerVolume", settings.get("audio.announcer"));
			general.setValue("EnableAudio", settings.get("audio.enabled"));
			
			//Interface settings
			hud.setValue("UseHUD2012", settings.get("hud.hudNew"));
			hud.setValue("FoundryItem", settings.get("hud.hudNew"));
			
			hud.setValue("DrawCenterHudWall", settings.get("hud.drawWall"));
			performance.setValue("EnableHUDAnimations", settings.get("hud.animations"));
			
			hud.setValue("ObjectTooltips", settings.get("hud.newbieTips"));
			hud.setValue("DrawHealthBars", settings.get("hud.healthBars"));
			hud.setValue("NumericCooldownFormat", settings.get("hud.cooldownDisplay"));
			
			hud.setValue("ItemShopItemDisplayMode", settings.get("hud.shopDisplayMode"));
			hud.setValue("ItemShopStartPane", settings.get("hud.shopStartPane"));
			
			hud.setValue("FlipMiniMap", settings.get("hud.flipMinimap"));
			
			hud.setValue("ChatScale", settings.get("hud.chatScale"));
			hud.setValue("ShowAllChannelChat", settings.get("hud.showAllChat"));
			hud.setValue("ShowTimestamps", settings.get("hud.showTimestamps"));
			
			//Game settings
			boolean colorblind = settings.getBoolean("game.colorblind");
			color.setValue("ColorPalette", colorblind ? "2" : "0");
			color.setValue("ColorPaletteSpectator", colorblind ? "2" : "0");
			hud.setValue("FlashScreenWhenDamaged", settings.get("game.damageFlash"));
			
			general.setValue("PredictMovement", settings.get("game.movePredict"));
			general.setValue("EnableLineMissileVis", settings.get("game.lineMissileDisplay"));
			general.setValue("SmartCastOnKeyRelease", settings.get("game.smartcastRange"));
			general.setValue("MapScrollSpeed", settings.get("game.scrollSpeed"));
			general.setValue("ScrollSmoothingEnabled", settings.get("game.scrollSmoothing"));
			
			floatingText.setValue("Legacy_Enabled", settings.get("float.legacy")); //Legacy
			floatingText.setValue("LegacyDamage_Enabled", settings.getBoolean("float.legacy") ? settings.get("float.damage") : "0");
			floatingText.setValue("PhysicalDamage_Enabled", settings.get("float.damage")); //Damage
			floatingText.setValue("MagicalDamage_Enabled", settings.get("float.damage"));
			floatingText.setValue("TrueDamage_Enabled", settings.get("float.damage"));
			floatingText.setValue("EnemyPhysicalDamage_Enabled", settings.get("float.enemyDamage")); //Enemy damage
			floatingText.setValue("EnemyMagicalDamage_Enabled", settings.get("float.enemyDamage"));
			floatingText.setValue("EnemyTrueDamage_Enabled", settings.get("float.enemyDamage"));
			floatingText.setValue("Critical_Enabled", settings.get("float.crit")); //Crits
			floatingText.setValue("LegacyCritical_Enabled", settings.getBoolean("float.legacy") ? settings.get("float.crit") : "0");
			floatingText.setValue("EnemyCritical_Enabled", settings.get("float.enemyCrit")); //Enemy crits
			floatingText.setValue("Heal_Enabled", settings.get("float.heal")); //Heal
			floatingText.setValue("Dodge_Enabled", settings.get("float.dodge")); //Dodge
			floatingText.setValue("Gold_Enabled", settings.get("float.gold")); //Gold
			floatingText.setValue("GoldOther_Enabled", settings.get("float.allyGold")); //Ally gold
			floatingText.setValue("Level_Enabled", settings.get("float.level")); //Level
			floatingText.setValue("Disable_Enabled", settings.get("float.status")); //Status
			floatingText.setValue("Invulnerable_Enabled", settings.get("float.status"));
			floatingText.setValue("Countdown_Enabled", settings.get("float.status"));
			floatingText.setValue("Special_Enabled", settings.get("float.special")); //Special
			floatingText.setValue("QuestReceived_Enabled", settings.get("float.quest")); //Quest
			floatingText.setValue("QuestComplete_Enabled", settings.get("float.quest"));
			floatingText.setValue("Score_Enabled", settings.get("float.score")); //Score
			floatingText.setValue("ManaDamage_Enabled", settings.get("float.mana")); //Mana
			floatingText.setValue("ManaHeal_Enabled", settings.get("float.mana"));
			floatingText.setValue("Experience_Enabled", settings.get("float.experience")); //Experience
			
			return INIWriter.writeINI(configFile, data) && saveHudSettings(settings);
		}
		return false;
	}
	
	public static void loadHudSettings(Settings settings)
	{
		File configFile = getHudConfigFile(settings);
		writeToLog("Loading HUD config data from file: "+configFile.getAbsolutePath());
		INIData data = INIParser.loadINIFile(configFile);
		if(data != null)
		{
			INIGroup global = data.getGroup("Global");
			
			settings.add("hud.hudScale", "GlobalScale", global, "1.0");
			settings.add("hud.minimapScale", "MinimapScale", global, "1.0");
		}
		else
			writeToLog("Failed to load HUD settings from \""+configFile.getAbsolutePath()+"\"", LoggingType.WARNING);
	}
	
	private static boolean saveHudSettings(Settings settings)
	{
		File configFile = getHudConfigFile(settings);
		writeToLog("Saving HUD config data to file: "+configFile.getAbsolutePath());
		INIData data = INIParser.loadINIFile(configFile);
		if(data != null)
		{
			INIGroup globals = data.getGroup("Globals");
			if(globals == null)
				globals = data.addGroup("Globals");
			
			globals.setValue("GlobalScale", settings.get("hud.hudScale"));
			globals.setValue("MinimapScale", settings.get("hud.minimapScale"));
			
			return INIWriter.writeINI(configFile, data);
		}
		return false;
	}
	
	//Helper methods
	
	private static File getConfigFile()
	{
		File file = new File(GamePathUtil.getDir().getPath()+"/Config/game.cfg");
		if(file.exists())
			return file;
		file = new File(GamePathUtil.getConfigDir()+"game.cfg");
		if(file.exists())
			return file;
		writeToLog("Failed to find config file", LoggingType.WARNING);
		return null;
	}
	
	private static File getHudConfigFile(Settings settings)
	{
		return new File(GamePathUtil.getGameDir()+"DATA/menu/hud/hud"+settings.get("video.width")+"x"+settings.get("video.height")+".ini");
	}
}
